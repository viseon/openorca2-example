package ch.viseon.orca2.binding.components.image

import ch.viseon.orca2.binding.components.BuildResult
import ch.viseon.orca2.binding.components.ComponentPmBuilder
import ch.viseon.orca2.common.ModelId
import ch.viseon.orca2.common.PresentationModelBuilder
import ch.viseon.orca2.common.Source
import ch.viseon.orca2.common.util.File
import ch.viseon.orca2.example.common.controller.ComponentProperties
import ch.viseon.orca2.example.common.controller.asModelType
import ch.viseon.orca2.example.common.controller.asPropertyName

class ImageViewerPm {

  companion object {

    val MODEL_TYPE = "PICTURE_VIEWER".asModelType()

    fun create(modelId: ModelId): BuildResult {
      val builder = Builder(modelId)
      return builder.build()
    }
  }

  class Builder(modelId: ModelId) : ComponentPmBuilder {
    private val pmBuilder = PresentationModelBuilder(modelId, MODEL_TYPE) {
      property(ComponentProperties.PROP_COMPONENT_TYPE) {
        value = MODEL_TYPE.stringId
      }
    }

    override fun build(): BuildResult {
      return BuildResult(pmBuilder.modelId, listOf(pmBuilder.build(Source.CONTROLLER)))
    }

  }
}

class ImageViewerElementPm {

  companion object {

    val PROP_CONTENT = "content".asPropertyName()
    val MODEL_TYPE = "picture-grid-pm".asModelType()

    fun create(modelId: ModelId, file: File): BuildResult {
      val command = PresentationModelBuilder(modelId, MODEL_TYPE) {
        property("content".asPropertyName()) {
          value = file
        }
      }
              .build(Source.CONTROLLER)

      return BuildResult(modelId, listOf(command))
    }
  }
}