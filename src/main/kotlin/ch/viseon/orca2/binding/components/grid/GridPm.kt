package ch.viseon.orca2.binding.components.grid

import ch.viseon.orca2.binding.components.BuildResult
import ch.viseon.orca2.binding.components.ComponentPmBuilder
import ch.viseon.orca2.binding.components.panel.PanelPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.example.common.controller.ComponentProperties
import ch.viseon.orca2.example.common.controller.asModelType
import ch.viseon.orca2.example.common.controller.asPropertyName


class GridPm {


  companion object {

    const val TYPE = "GRID"

    val PROP_SELECTED_MODEL_ID = "selectedModelId".asPropertyName()
    val PROP_MODEL_TYPE = PropertyName("row-model-TYPE")
    val PROP_META_INFO = PropertyName("meta-info")

    fun create(modelId: ModelId, block: Builder.() -> Unit): BuildResult {
      val builder = Builder(modelId)
      builder.block()
      return builder.build()
    }

    fun getSelectionType(configModelId: ModelId): ModelType {
      return (configModelId.stringId + "Selection").asModelType()
    }

    fun getSelectionModelId(rowModelId: ModelId): ModelId {
      return rowModelId + "Selection"
    }

    fun createSelectionPm(rowModelId: ModelId, selectionType: ModelType, source: Source): CreateModelCommandData {
      return PresentationModelBuilder( getSelectionModelId(rowModelId), selectionType) {
        property(PROP_SELECTED_MODEL_ID) {
          value = rowModelId
        }
      }.build(source)
    }

  }

  class Builder(modelId: ModelId): ComponentPmBuilder {

    private val pmBuilder = PresentationModelBuilder(modelId, TYPE.asModelType()) {
      property(ComponentProperties.PROP_COMPONENT_TYPE) {
        value = TYPE
      }
    }

    private val commands = mutableListOf<CreateModelCommandData>()

    fun row(modelType: ModelType) {
      pmBuilder.property(PROP_MODEL_TYPE) {
        value = modelType
      }
    }

    fun metaInfo(modelId: ModelId) {
      pmBuilder.property(PROP_META_INFO) {
        value = modelId
      }
    }

    fun metaInfo(dataPm: ModelId, modelType: ModelType, block: PresentationModelBuilder.() -> Unit) {
      metaInfo(dataPm)
      commands.add(PresentationModelBuilder(dataPm, modelType, block).build(Source.CONTROLLER))
    }

    override fun build(): BuildResult {
      return BuildResult(pmBuilder.modelId, commands.plus(pmBuilder.build(Source.CONTROLLER)))
    }
  }
}