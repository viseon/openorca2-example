package ch.viseon.orca2.binding.components

import ch.viseon.orca2.common.CommandData
import ch.viseon.orca2.common.Tag
import ch.viseon.orca2.rx.Observable

typealias ComponentResult = Observable<List<CommandData>>

//Tags for rendering hint
val HTML_TAG = Tag("HTML")
