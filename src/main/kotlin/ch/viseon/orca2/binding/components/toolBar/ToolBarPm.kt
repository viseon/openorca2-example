package ch.viseon.orca2.binding

import ch.viseon.orca2.binding.components.BuildResult
import ch.viseon.orca2.binding.components.ComponentPmBuilder
import ch.viseon.orca2.common.*
import ch.viseon.orca2.example.common.controller.ComponentProperties
import ch.viseon.orca2.example.common.controller.asModelType
import ch.viseon.orca2.example.common.controller.asPropertyName

class ToolBarPm {

  companion object {

    const val TYPE = "TOOLBAR"

    fun create(modelId: ModelId, block: Builder.() -> Unit): BuildResult {
      val builder = Builder(modelId)
      builder.block()
      return builder.build()
    }

  }

  class Builder(modelId: ModelId) : ComponentPmBuilder {

    private val commands = mutableListOf<CommandData>()
    private val pmBuilder = PresentationModelBuilder(modelId, TYPE.asModelType()) {
      property(ComponentProperties.PROP_COMPONENT_TYPE) {
        value = TYPE
      }
    }

    override fun build(): BuildResult {
      return BuildResult(pmBuilder.modelId, commands.apply { add(pmBuilder.build(Source.CONTROLLER)) })
    }

    fun add(name: PropertyName, buildResult: BuildResult) {
      pmBuilder.property(name) {
        value = buildResult.modelId
      }
      commands.addAll(buildResult.commands)
    }

    fun add(buildResult: BuildResult) {
      add("component-${pmBuilder.propertyList.size}".asPropertyName(), buildResult)
    }
  }
}