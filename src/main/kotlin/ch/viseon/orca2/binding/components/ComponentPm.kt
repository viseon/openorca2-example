package ch.viseon.orca2.binding.components

import ch.viseon.orca2.common.CommandData
import ch.viseon.orca2.common.ModelId

interface ComponentPm {

  val modelId: ModelId
}

interface ComponentPmBuilder {

  fun build(): BuildResult

}
data class BuildResult(val modelId: ModelId, val commands: List<CommandData>)