package ch.viseon.orca2.rx


expect object ObservableOperators {

  fun <T> merge(vararg observable: Observable<T>): Observable<T>
}