package ch.viseon.orca2.rx


expect abstract class Observable<T>  {}

expect fun <T> Observable<T>.subscribe(block: (T) -> Unit): Disposable

expect fun <T> Observable<T>.subscribe(block: (T) -> Unit, errorBlock: (Throwable) -> Unit): Disposable

expect fun <T> Observable<T>.filter(block: (T) -> Boolean): Observable<T>

expect fun <T,R> Observable<T>.map(block: (T) -> R): Observable<R>

expect fun <T,R> Observable<T>.flatMap(block: (T) -> Observable<R>): Observable<R>

expect fun <R> Observable<R>.onErrorResumeNext(block: (Throwable) -> Observable<R>): Observable<R>

expect fun <T, R> Observable<T>.reduceToObservable(seed: R, block: (R, T) -> R): Observable<R>

expect fun <T> Observable<T>.collect(): Observable<List<T>>

expect interface Disposable {

  fun dispose()

  fun isDisposed(): Boolean

}
