package ch.viseon.orca2.rx

expect object ObservableFactory {

  fun <T> create(producer: (CustomSubscriber<T>) -> Unit): Observable<T>

  fun <T> empty(): Observable<T>

  fun <T> of(t: T): Observable<T>
}
