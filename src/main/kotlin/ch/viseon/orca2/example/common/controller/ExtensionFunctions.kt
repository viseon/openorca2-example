package ch.viseon.orca2.example.common.controller

import ch.viseon.orca2.common.ModelId
import ch.viseon.orca2.common.ModelType
import ch.viseon.orca2.common.PropertyName

fun String.asModelType(): ModelType {
  return ModelType(this)
}

fun String.asModelId(): ModelId {
  return ModelId(this)
}

fun String.asPropertyName(): PropertyName {
  return PropertyName(this)
}