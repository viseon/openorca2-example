package ch.viseon.orca2.example.common.controller

import ch.viseon.orca2.common.PropertyName

object ComponentProperties {
  val PROP_COMPONENT_TYPE = PropertyName("component-TYPE")
}

