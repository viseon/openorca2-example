package ch.viseon.orca2.common.network

import ch.viseon.orca2.rx.Observable


expect class NetworkConnection(host: String) {

  fun post(request: Request): Observable<Response>

  fun delete(request: Request): Observable<Response>

  fun get(request: Request): Observable<Response>

}

class Request(val path: String, val data: RequestData, val responseType: ResponseType = ResponseType.JSON)

class Response(val statusCode: Int, val data: Any) {

  val isOk = statusCode == 200 || statusCode == 204
}

enum class ResponseType {
  /**
   * Response is a string
   */
  JSON,
  /**
   * Response is a Blob
   */
  BINARY
}
