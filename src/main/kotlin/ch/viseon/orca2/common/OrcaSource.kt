/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.orca2.common

import ch.viseon.orca2.rx.Observable

expect class OrcaSource {

  /**
   * Observes the model store for changes.
   */
  fun observeModelStore(): Observable<ModelStoreChangeEvent>

  /**
   * Observes a model.
   */
  fun observeModel(modelId: ModelId): Observable<PropertyChangeEvent>

  /**
   * Observes all model with the given type.
   */
  fun observeModel(modelType: ModelType): Observable<PropertyChangeEvent>

  /**
   * Observes a particular property of a model.
   */
  fun observeProperty(modelId: ModelId, propertyName: PropertyName): Observable<ValueChangeEvent>

  /**
   * Observe a action that was send from the client.
   */
  fun registerNamedCommand(actionName: String): Observable<ActionEvent>

  fun contains(modelId: ModelId): Boolean
  fun contains(modelType: ModelType): Boolean

  fun model(modelId: ModelId): PresentationModel
  fun models(modelType: ModelType): Collection<PresentationModel>

  fun getAllModels(): Collection<PresentationModel>

  fun processCommands(commands: List<CommandData>)
}

