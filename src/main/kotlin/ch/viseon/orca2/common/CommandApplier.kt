/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.orca2.common


data class CommandApplication(val applied: Boolean, val events: Iterable<Event>, val commandData: CommandData) {
  companion object {
    fun applied(events: Iterable<Event>, commandData: CommandData): CommandApplication {
      return CommandApplication(true, events, commandData)
    }

    fun rejected(commandData: CommandData): CommandApplication {
      return CommandApplication(false, listOf(), commandData)
    }
  }
}


object CommandApplier {

  private val executors = mapOf(
      CreateModelCommandData::class.simpleName!! to CreateModelExecutor,
      RemoveModelCommandData::class.simpleName!! to RemoveModelExecutor,
      ActionCommandData::class.simpleName!! to ActionExecutor,
      ChangeValueCommandData::class.simpleName!! to ChangeValueExecutor,
      RemoveModelByTypeCommandData::class.simpleName!! to RemoveModelByTypeExecutor,
      SyncModelCommandData::class.simpleName!! to SyncModelExecutor
  )

  fun apply(source: Source, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    val executor = executors[commandData::class.simpleName!!] ?: throw IllegalArgumentException("No executor found for commandData: '$commandData'")
    try {
      return executor.execute(source, modelStore, commandData)
    } catch (e: Exception) {
      throw RuntimeException("Failed to apply command: '$commandData'. Original error message: '${e.message}'")
    }
  }

  fun apply(source: Source, modelStore: ModelStore, commandData: List<CommandData>): List<CommandApplication> {
    return commandData.map { apply(source, modelStore, it) }
  }

}

interface CommandExecutor {
  fun execute(source: Source, modelStore: ModelStore, commandData: CommandData): CommandApplication

  fun forceApply(ourSource: Source, commandData: CommandData): Boolean {
    return commandData.policy.isForceApply() || (ourSource != commandData.source && commandData.policy.isForceSend())
  }
}

private object CreateModelExecutor : CommandExecutor {

  override fun execute(source: Source, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    //LOGGER.finer { "applyingCommand data: $commandData" }

    val it = commandData as CreateModelCommandData

    if (!forceApply(source, it) && modelStore.contains(commandData.modelId)) {
      return CommandApplication.rejected(commandData)
    }

    val model = DefaultPresentationModel(it.modelId, it.modelType, it.properties.asSequence())
    modelStore.addModel(model)
    return CommandApplication.applied(
        listOf(ModelStoreChangeEvent(it.source, model.id, model.type, ModelStoreChangeEventType.ADD)),
        commandData
    )
  }
}

private object ChangeValueExecutor : CommandExecutor {

  override fun execute(source: Source, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    //LOGGER.finer { "Execute ChangeValue. Data: '$commandData" }

    val it = commandData as ChangeValueCommandData

    //wenn das command keine Wirkung muss es nicht ausgeführt werden.
    //---> Ausser es ist forceSend von der anderen Seite.
    if (!forceApply(source, it) && modelStore[commandData.modelId][commandData.propertyName].let { it.hasValue(commandData.tag) && it[commandData.tag] == commandData.value }) {
      return CommandApplication.rejected(commandData)
    }

    val model = modelStore[it.modelId]
    val oldValue = model[it.propertyName].set(it.tag, it.value)
    return CommandApplication.applied(
        listOf(PropertyChangeEvent(it.source, model.id, model.type, ValueChangeEvent(it.source, it.propertyName, it.tag, oldValue, it.value))),
        commandData
    )

  }

}

private object ActionExecutor : CommandExecutor {

  override fun execute(source: Source, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    //LOGGER.finer { "Execute Action. Data: '$commandData" }

    val it = commandData as ActionCommandData
    return CommandApplication.applied(
        listOf(ActionEvent(it.source, it.actionName, it.modelIds)),
        commandData
    )
  }

}

private object RemoveModelExecutor : CommandExecutor {

  override fun execute(source: Source, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    //LOGGER.finer { "Execute RemoveModel. Data: '$commandData" }
    val it = commandData as RemoveModelCommandData

    if (!forceApply(source, it) && it.modelId !in modelStore) {
      return CommandApplication.rejected(commandData)
    }

    if (it.modelId in modelStore) modelStore.removeModel(it.modelId)

    return CommandApplication.applied(
        listOf(ModelStoreChangeEvent(it.source, it.modelId, it.modelType, ModelStoreChangeEventType.REMOVE)),
        commandData
    )
  }

}

private object RemoveModelByTypeExecutor : CommandExecutor {

  override fun execute(source: Source, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    //LOGGER.finer { "Execute RemoveModelByType. Data: '$commandData" }
    val it = commandData as RemoveModelByTypeCommandData

    if (!forceApply(source, it) && it.modelType !in modelStore) {
      return CommandApplication.rejected(commandData)
    }

    return CommandApplication.applied(
        modelStore.removeModels(it.modelType).map { pm -> ModelStoreChangeEvent(it.source, pm.id, pm.type, ModelStoreChangeEventType.REMOVE) },
        commandData
    )
  }

}

private object SyncModelExecutor : CommandExecutor {

  override fun execute(source: Source, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    //LOGGER.finer { "Execute SyncModel. Data: '$commandData" }

    val data = commandData as SyncModelCommandData
    val sourcePm = modelStore[data.sourceModel]
    val destinationPm = modelStore[data.destinationModel]
    val events = sourcePm.getProperties()
        .filter { property -> destinationPm.hasProperty(property.name) }
        .flatMap { property: Property ->
          property.getValues()
              .map { pair: Pair<Tag, Any> ->
                val (tag, value) = pair
                //Values are immutable
                val oldValue = destinationPm[property.name].set(tag, value)
                PropertyChangeEvent(data.source, destinationPm.id, destinationPm.type, ValueChangeEvent(data.source, property.name, tag, oldValue, value))
              }
        }
        .toList()
    return CommandApplication.applied(events, commandData)
  }

}

