/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.orca2.common

enum class ApplyPolicy {

  DEFAULT,
  /**
   * Forces this side to send this command to the other side. The command will only forcefully applied on the other side.
   */
  FORCE_SEND,

  /**
   * Forces the application of this command and forcefully sends it to the other side.
   */
  FORCE_APPLY;

  fun isForceSend(): Boolean {
    return this == FORCE_SEND || isForceApply()
  }

  fun isForceApply(): Boolean {
    return this == FORCE_APPLY
  }

  fun isDefault(): Boolean {
    return this == DEFAULT
  }
}

/**
 * Command data serves as a carrier of a change: Create a new model, or change a value from a particular model.
 *
 * Clients can implement there on commands. Check serialization of commands from a Request and deserialization to a Response.
 */
interface CommandData {

  val source: Source

  val policy: ApplyPolicy
}

data class ChangeValueCommandData(override val source: Source,
                                  val modelId: ModelId,
                                  val propertyName: PropertyName,
                                  val tag: Tag,
                                  val value: Any,
                                  override val policy: ApplyPolicy = ApplyPolicy.DEFAULT
                                  ) : CommandData

data class CreateModelCommandData(override val source: Source,
                                  val modelId: ModelId,
                                  val modelType: ModelType,
                                  val properties: Collection<Property>,
                                  override val policy: ApplyPolicy = ApplyPolicy.DEFAULT) : CommandData

data class RemoveModelCommandData(override val source: Source,
                                  val modelId: ModelId,
                                  val modelType: ModelType,
                                  override val policy: ApplyPolicy = ApplyPolicy.DEFAULT) : CommandData {

  constructor(source: Source, pm: PresentationModel): this(source, pm.id, pm.type)

}



data class ActionCommandData(override val source: Source,
                             val actionName: String,
                             val modelIds: List<ModelId> = listOf(),
                             override val policy: ApplyPolicy = ApplyPolicy.DEFAULT) : CommandData {
  constructor(source: Source, actionName: String, pm: PresentationModel, policy: ApplyPolicy = ApplyPolicy.DEFAULT)
          : this(source, actionName, listOf(pm.id), policy)
}


/**
 * Removes all models of a certain type.
 */
data class RemoveModelByTypeCommandData(
    override val source: Source,
    val modelType: ModelType,
    override val policy: ApplyPolicy = ApplyPolicy.DEFAULT) : CommandData

/**
 * Copy one model onto another.
 */
data class SyncModelCommandData(
    override val source: Source,
    val sourceModel: ModelId,
    val destinationModel: ModelId,
    override val policy: ApplyPolicy = ApplyPolicy.DEFAULT) : CommandData


object CommandUtils {
  fun changeValue(source: Source, pm: PresentationModel, propertyName: PropertyName, value: Any): ChangeValueCommandData {
    return ChangeValueCommandData(source, pm.id, propertyName, Tag.VALUE, value)
  }

  fun changeValueController(modelId: ModelId, propertyName: PropertyName, value: Any): ChangeValueCommandData {
    return ChangeValueCommandData(Source.CONTROLLER, modelId, propertyName, Tag.VALUE, value)
  }

  fun changeValueUI(modelId: ModelId, propertyName: PropertyName, value: Any): ChangeValueCommandData {
    return ChangeValueCommandData(Source.UI, modelId, propertyName, Tag.VALUE, value)
  }
}
