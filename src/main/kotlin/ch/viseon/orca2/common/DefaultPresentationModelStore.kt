/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.orca2.common

class DefaultPresentationModelStore : ModelStore {

  private val models = LinkedHashMap<ModelId, PresentationModel>()
  private val modelType2Model = HashMap<ModelType, MutableList<PresentationModel>>()

  override fun addModel(model: PresentationModel) {
    models[model.id] = model
    modelType2Model.getOrPut(model.type, { ArrayList() }).add(model)
  }

  override operator fun get(modelId: ModelId): PresentationModel {
    return models[modelId] ?: throw IllegalArgumentException("No model with id '$modelId' present.")
  }

  override operator fun get(modelType: ModelType): Collection<PresentationModel> {
    return modelType2Model[modelType] ?: listOf()
  }

  override fun removeModel(modelId: ModelId): PresentationModel {
    val removedPm = models.remove(modelId)!!
    modelType2Model[removedPm.type]?.remove(removedPm)
    return removedPm
  }

  override fun removeModels(modelType: ModelType): Collection<PresentationModel> {
    val removedPms = modelType2Model.remove(modelType)
    removedPms?.forEach { models.remove(it.id) }
    return removedPms ?: emptyList()
  }

  operator override fun contains(modelId: ModelId): Boolean {
    return models.contains(modelId)
  }

  operator override fun contains(modelType: ModelType): Boolean {
    return (modelType2Model[modelType]?.size ?: 0) > 0
  }

  override fun getAllModels(): Collection<PresentationModel> {
    return models.values
  }

}

