package ch.viseon.orca2.common.util


expect object PlatformFileUtil {

  fun sha256(fileData: ByteArray): String

  fun createFile(data: ByteArray, fileName: String, fileType: String): File

}

expect fun myPrintStackTrace(throwable: Throwable)


