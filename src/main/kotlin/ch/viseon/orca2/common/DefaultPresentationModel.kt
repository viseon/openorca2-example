/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.orca2.common

class DefaultPresentationModel(override val id: ModelId,
                               override val type: ModelType,
                               properties: Sequence<Property>) : PresentationModel {

  private val properties = LinkedHashMap<PropertyName, Property>().apply {
    properties.forEach { property -> this[property.name] = property }
  }

  override fun getPropertiesArray(): Array<Property> {
    return properties.values.toTypedArray()
  }

  override fun getProperties(): Sequence<Property> {
    return properties.values.asSequence()
  }

  override fun propertyCount(): Int {
    return properties.size
  }

  override operator fun get(propertyName: PropertyName): Property {
    return properties[propertyName] ?: throw IllegalArgumentException("Property '$propertyName' not found.")
  }

  override fun toString(): String {
    return "${properties.map { it.value.toString() }}"
  }

  override fun hasProperty(name: PropertyName): Boolean {
    return properties.containsKey(name)
  }
}