/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.orca2.common

class Property(val name: PropertyName, initialValues: Sequence<Pair<Tag, Any>>) {

  private val propertyValue2Values: MutableMap<Tag, Any> = HashMap<Tag, Any>().apply {
    initialValues.forEach { (key, value) -> this[key] = value }
  }

  /**
   * @return the oldValue of the given propertyValue.
   */
  operator fun set(tag: Tag, value: Any): Any? {
    checkIfValueCanBeSent(tag, value)
    val oldValue: Any? = propertyValue2Values[tag]
    propertyValue2Values[tag] = value
    return oldValue
  }

  private fun checkIfValueCanBeSent(tag: Tag, value: Any) {
//    if (!isSupported(value)) {
//      throw IllegalArgumentException("Not supported value: '$value'. Property: '$name' for Tag '$tag'")
//    }
  }

  fun getValuesArray(): Array<Pair<Tag, Any>> {
    return propertyValue2Values.entries.asSequence().map { Pair(it.key, it.value) }.toList().toTypedArray()
  }

  fun getValues(): Sequence<Pair<Tag, Any>> {
    return propertyValue2Values.entries.asSequence().map { Pair(it.key, it.value) }
  }

  operator fun get(tag: Tag = Tag.VALUE): Any {
    return propertyValue2Values[tag] ?: throw IllegalArgumentException("No value for tag '$tag'")
  }

  inline fun <reified T : Any> getValue(tag: Tag = Tag.VALUE): T {
    val get = get(tag)
    return get as? T ?: throw IllegalArgumentException("Value is not of type ${T::class}. Actual value '$get' Type: ${get.let { it::class.simpleName }}")
  }

  override fun toString(): String {
    return "$name: values: $propertyValue2Values"
  }

  fun hasValue(tag: Tag = Tag.VALUE): Boolean {
    return propertyValue2Values.containsKey(tag)
  }

}
