package ch.viseon.orca2.common

object Foo {

  fun get(): ch.viseon.orca2.rx.Observable<String> {
    return ch.viseon.orca2.rx.ObservableFactory.create {
      it.onNext("1. Message from Obs")
      it.onNext("2. Message from Obs")
      it.onNext("3. Message from Obs")
      it.onNext("4. Message from Obs")
    }
  }
}

