package ch.viseon.orca2.common

import ch.viseon.orca2.common.util.myPrintStackTrace
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.subscribe


class OrcaRun(val orcaSource: OrcaSource) {

  fun register(block: (OrcaSource) -> Observable<List<CommandData>>) {
    val commands = block(orcaSource)
    commands.subscribe({
      orcaSource.processCommands(it)
    }, {
      val exception: Throwable = it as Throwable
      myPrintStackTrace(exception)
    })
  }

  //Needed, because kotlin compiler has a bug with typealias actual implementations
  @Deprecated("Needed because kotlin compiler 1.2.41 has a bug", replaceWith = ReplaceWith("this.register(block)"))
  fun registerBuggyCompiler(block: (OrcaSource) -> Any) {
    register { block(orcaSource) as Observable<List<CommandData>> }
  }

  fun processCommands(commands: List<CommandData>) {
    orcaSource.processCommands(commands)
  }

}