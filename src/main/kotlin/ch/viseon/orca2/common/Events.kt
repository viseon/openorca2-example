/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.orca2.common

interface Event {
  val source: Source
}

enum class ModelStoreChangeEventType {
  ADD,
  REMOVE,
  ;

  fun isAdd() = this == ADD
  fun isRemove() = this == REMOVE
}

data class ModelStoreChangeEvent(
    override val source: Source,
    val modelId: ModelId, val modelType: ModelType, val eventType: ModelStoreChangeEventType) : Event

data class PropertyChangeEvent(
    override val source: Source,
    val modelId: ModelId, val modelType: ModelType, val valueChangeEvent: ValueChangeEvent) : Event

data class ValueChangeEvent(
    override val source: Source, val property: PropertyName, val tag: Tag, val oldValue: Any?, val newValue: Any) : Event {


  fun isValueChange() = tag == Tag.VALUE
  fun isLabelChange() = tag == Tag.LABEL
  fun isToolTipChange() = tag == Tag.TOOL_TIP
}

data class ActionEvent(
    override val source: Source, val actionName: String, val pmIds: Iterable<ModelId>) : Event

