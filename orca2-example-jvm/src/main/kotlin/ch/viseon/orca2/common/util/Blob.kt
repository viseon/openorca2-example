package ch.viseon.orca2.common.util

import ch.viseon.orca2.rx.Observable
import io.reactivex.Single
import java.nio.file.Files

/**
 * Represents an abstraction over a file.
 */
actual open class Blob(val data: ByteArray, actual val type: String) {
  actual val size: Int
    get() = data.size

}

actual fun Blob.useAsByteArray(block: (ByteArray) -> Unit) {
  return block(this.data)
}

actual class File(data: ByteArray, type: String, actual val name: String): Blob(data, type)

actual fun Blob.emitByteArray(): Observable<ByteArray> {
  return Single.just(this.data).toObservable()
}

fun java.io.File.toCommonFile(): File {
  val data = Files.readAllBytes(this.toPath())
  //FIXME: Introduce propper MIME-Type handling
  return File(data, "image/png", this.name)
}