package ch.viseon.orca2.common.network

import ch.viseon.orca2.common.util.File
import ch.viseon.orca2.rx.Observable
import com.github.kittinunf.fuel.core.*
import com.github.kittinunf.fuel.core.deserializers.ByteArrayDeserializer
import com.github.kittinunf.fuel.core.deserializers.StringDeserializer
import com.github.kittinunf.fuel.rx.rx_response
import com.github.kittinunf.fuel.rx.rx_responseObject
import com.github.kittinunf.result.Result
import kotlinx.io.ByteArrayInputStream
import kotlinx.io.ByteBuffer
import kotlinx.io.ByteOrder
import kotlinx.serialization.internal.readToByteBuffer


actual class NetworkConnection actual constructor(private val host: String) {

  private val manager = FuelManager().also {
    it.basePath = host
  }

  actual fun post(request: Request): Observable<Response> {
    return manager
            .request(Method.POST, request.path)
            .setRequestData(request.data)
            .rx_responseObject(StringDeserializer())
            .map { (fuelResponse, result) ->
              when (result) {
                is Result.Success -> Response(fuelResponse.statusCode, result.value)
                is Result.Failure -> {
                  result.error.printStackTrace()
                  Response(fuelResponse.statusCode, result.error.localizedMessage)
                }
              }
            }
            .toObservable() as Observable<Response>
  }

  actual fun delete(request: Request): Observable<Response> {
    return manager
            .request(Method.DELETE, request.path)
            .setRequestData(request.data)
            .rx_response()
            .doOnError { println(it) }
            .map { (fuelResponse, result) ->
              when (result) {
                is Result.Success -> Response(fuelResponse.statusCode, "")
                is Result.Failure -> {
                  result.error.printStackTrace()
                  Response(fuelResponse.statusCode, result.error.localizedMessage)
                }
              }
            }
            .toObservable() as Observable<Response>
  }

  actual fun get(request: Request): Observable<Response> {
    return manager
            .request(Method.GET, request.path)
            .rx_responseObject(if (request.responseType == ResponseType.BINARY) KotlinByteBufferDeserializer() else StringDeserializer())
            .map { (fuelResponse, result) ->
              when (result) {
                is Result.Success -> {
                  Response(fuelResponse.statusCode, result.value)
                }
                is Result.Failure -> {
                  result.error.printStackTrace()
                  Response(fuelResponse.statusCode, result.error.localizedMessage)
                }
              }
            }
            .toObservable() as Observable<Response>
  }

  private fun com.github.kittinunf.fuel.core.Request.setRequestData(data: RequestData): com.github.kittinunf.fuel.core.Request {
    when (data) {
      is JsonData<*> -> body(data.jsonString)
      is EmptyData -> {
      }
      is BinaryData -> body(data.data)
      is MultipartData -> {
        this.request.type = com.github.kittinunf.fuel.core.Request.Type.UPLOAD
        this.request.headers["Content-Type"] = "multipart/form-data; boundary=${System.currentTimeMillis()}"
        blobs { request, url ->
          data.parts.entries.map { (key, value) ->
            val dataFile = (value as File)
            Blob(key, dataFile.size.toLong()) {ByteArrayInputStream(dataFile.data)}
          }
        }
      }
    }
    return this
  }

}

private class KotlinByteBufferDeserializer : Deserializable<ByteBuffer> {

  private val internalDeserializer = ByteArrayDeserializer()

  override fun deserialize(response: com.github.kittinunf.fuel.core.Response): ByteBuffer {
    val byteArray = internalDeserializer.deserialize(response)
    return ByteArrayInputStream(byteArray).use {
      val buffer = it.readToByteBuffer(byteArray.size)
      buffer.order(ByteOrder.LITTLE_ENDIAN)
    }
  }

}
