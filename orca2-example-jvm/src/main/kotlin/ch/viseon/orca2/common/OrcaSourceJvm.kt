package ch.viseon.orca2.common

import ch.viseon.orca2.rx.Observable
import io.reactivex.subjects.PublishSubject

actual class OrcaSource(private val modelStore: DefaultPresentationModelStore) {

  private val eventSubject = PublishSubject.create<Event>()

  actual fun observeModelStore(): Observable<ModelStoreChangeEvent> {
    return eventSubject
            .filter { it is ModelStoreChangeEvent }
            .map { it as ModelStoreChangeEvent } as ch.viseon.orca2.rx.Observable<ModelStoreChangeEvent>
  }

  actual fun observeModel(modelId: ModelId): Observable<PropertyChangeEvent> {
    return eventSubject
            .filter { it is PropertyChangeEvent }
            .map { it as PropertyChangeEvent }
            .filter { it.modelId == modelId }
  }

  actual fun observeModel(modelType: ModelType): Observable<PropertyChangeEvent> {
    return eventSubject
            .filter { it is PropertyChangeEvent }
            .map { it as PropertyChangeEvent }
            .filter { it.modelType == modelType }
  }

  actual fun observeProperty(modelId: ModelId, propertyName: PropertyName): Observable<ValueChangeEvent> {
    return eventSubject
            .filter { it is PropertyChangeEvent }
            .map { it as PropertyChangeEvent }
            .filter { it.modelId == modelId }
            .filter { it.valueChangeEvent.property == propertyName }
            .map { it.valueChangeEvent }
  }

  actual fun registerNamedCommand(actionName: String): Observable<ActionEvent> {
    return eventSubject
            .filter { it is ActionEvent }
            .map { it as ActionEvent }
            .filter { it.actionName == actionName }
  }

  actual fun contains(modelId: ModelId): Boolean {
    return modelStore.contains(modelId)
  }

  actual fun contains(modelType: ModelType): Boolean {
    return modelStore.contains(modelType)
  }

  actual fun model(modelId: ModelId): PresentationModel {
    return modelStore[modelId]
  }

  actual fun models(modelType: ModelType): Collection<PresentationModel> {
    return modelStore[modelType]
  }

  actual fun getAllModels(): Collection<PresentationModel> {
    return modelStore.getAllModels()
  }

  actual fun processCommands(commands: List<CommandData>) {
    val applications = CommandApplier.apply(Source.UI, modelStore, commands)
    if (applications.isEmpty()) {
      return
    }
    processEvents(applications)
  }

  private fun processEvents(commandApplications: Iterable<CommandApplication>) {
    return commandApplications.forEach { commandApplication ->
      commandApplication.events.forEach { event ->
        processEvent(event)
      }
    }
  }

  private fun processEvent(event: Event) {
    eventSubject.onNext(event)
  }

}