package ch.viseon.orca2.example.ui.swing

import ch.viseon.orca2.binding.components.panel.PanelLayout
import ch.viseon.orca2.binding.components.panel.PanelPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.example.common.controller.ComponentProperties
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.ObservableOperators
import java.awt.Container
import javax.swing.JPanel
import javax.swing.BoxLayout



class SwingPanelComponent {

  companion object {
    fun create(configModelId: ModelId, orcaRun: OrcaRun, parent: Container): Observable<List<CommandData>> {
      val configModel = orcaRun.orcaSource.model(configModelId)

      val axis = getLayout(configModel)
      val panel = JPanel()
      panel.layout = BoxLayout(panel, axis)
      parent.add(panel)

      return renderViewAndCollectIntent(configModel, orcaRun, panel)
    }

    private fun getLayout(configModel: PresentationModel): Int {
      val layoutValue = (configModel[PanelPm.PROP_LAYOUT].get() as? Number)?.toInt() ?: 0
      val layout = PanelLayout.values()[layoutValue]
      return if (layout == PanelLayout.HORIZONTAL) BoxLayout.X_AXIS else BoxLayout.Y_AXIS
    }

    private fun renderViewAndCollectIntent(configModel: PresentationModel, orcaRun: OrcaRun, container: Container): Observable<List<CommandData>> {
      val intents = configModel
              .getProperties()
              .asSequence()
              .filter { it.hasValue(Tag.VALUE) }
              .map { it[Tag.VALUE] }
              .filter { it is ModelId }
              .map { it as ModelId }
              .filter { orcaRun.orcaSource.contains(it) && orcaRun.orcaSource.model(it).hasProperty(ComponentProperties.PROP_COMPONENT_TYPE)  }
              .map {  ComponentFactory.createComponent(orcaRun.orcaSource.model(it), orcaRun, container)  }
              .toList()

      return ObservableOperators.merge(*intents.toTypedArray())
    }

  }


}
