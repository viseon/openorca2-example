package ch.viseon.orca2.example.ui.swing

import ch.viseon.orca2.binding.components.ComponentResult
import ch.viseon.orca2.binding.components.image.ImageViewerElementPm
import ch.viseon.orca2.binding.components.image.AbstractImageViewer
import ch.viseon.orca2.common.*
import ch.viseon.orca2.common.util.File
import java.awt.Color
import java.awt.Container
import java.awt.Dimension
import java.awt.FlowLayout
import java.io.ByteArrayInputStream
import javax.imageio.ImageIO
import javax.swing.ImageIcon
import javax.swing.JLabel
import javax.swing.JPanel


class SwingImageViewer(override val orcaSource: OrcaSource): AbstractImageViewer<Container>() {

  override fun createContainerElement(containerParent: Container): Container {
    val container = JPanel().apply {
      this.layout = FlowLayout()
      this.background = Color.LIGHT_GRAY
      this.preferredSize = Dimension(200, 200)
    }

    containerParent.add(container)
    return container
  }

  override fun createImageView(presentationModel: PresentationModel, parent: Container) {
    val fileContent = presentationModel[ImageViewerElementPm.PROP_CONTENT].getValue<File>()

    JLabel().also { label ->
      val image = ByteArrayInputStream(fileContent.data).use {
        ImageIO.read(it).getScaledInstance(100, 100, 0)
      }

      label.icon = ImageIcon(image)

      parent.add(label)
      parent.revalidate()
      parent.repaint()
    }
  }

  companion object {
    fun create(modelId: ModelId, orcaRun: OrcaRun, parent: Container): ComponentResult {
      return SwingImageViewer(orcaRun.orcaSource).createUI(parent)
    }

  }

}