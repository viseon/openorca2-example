package ch.viseon.orca2.example.ui.swing

import ch.viseon.orca2.binding.components.button.ButtonPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.rx.Observable
import io.reactivex.subjects.PublishSubject
import java.awt.Container
import javax.swing.JButton
import javax.swing.JComponent

class SwingButtonComponent {

  companion object {
    fun create(modelId: ModelId, orcaRun: OrcaRun, parent: Container): Observable<List<CommandData>> {
      val subject = PublishSubject.create<List<CommandData>>()
      val configModel = orcaRun.orcaSource.model(modelId)
      val component = generateView(configModel, orcaRun, subject)
      parent.add(component)
      parent.doLayout()
      return subject
    }

    private fun generateView(configModel: PresentationModel, orcaRun: OrcaRun, subject: PublishSubject<List<CommandData>>): JComponent {
      val button = JButton(configModel[ButtonPm.PROP_ACTION][Tag.LABEL] as String)

      val actionName = configModel[ButtonPm.PROP_ACTION].get().toString()

      button.addActionListener {

        subject.onNext(listOf(ActionCommandData(Source.UI, actionName, listOf(configModel.id))))
      }

      return button
    }

  }

}