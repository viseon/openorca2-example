package ch.viseon.orca2.example.ui.swing

import java.util.ArrayList
import javax.swing.ListSelectionModel
import javax.swing.RowSorter

object SwingUtil {

  fun getSelectedRows(selectionModel: ListSelectionModel, sorter: RowSorter<*>): ArrayList<Int> {
    val resultList = ArrayList<Int>()
    (selectionModel.minSelectionIndex..selectionModel.maxSelectionIndex).forEach {
      if (selectionModel.isSelectedIndex(it)) {
        if (it < sorter.viewRowCount) {
          val modelIndex = sorter.convertRowIndexToModel(it)
          resultList.add(modelIndex)
        }
      }
    }

    return resultList
  }
}