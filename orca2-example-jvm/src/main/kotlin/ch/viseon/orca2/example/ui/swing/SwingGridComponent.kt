package ch.viseon.orca2.example.ui.swing

import ch.viseon.orca2.binding.components.grid.GridPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.rx.Observable
import io.reactivex.subjects.PublishSubject
import java.awt.Container
import javax.swing.JScrollPane
import javax.swing.JTable
import javax.swing.ListSelectionModel
import javax.swing.RowSorter
import javax.swing.table.AbstractTableModel

//FIXME: Handle component disposing
class SwingGridComponent {

  companion object {
    fun create(modelId: ModelId, orcaRun: OrcaRun, parent: Container): Observable<List<CommandData>> {

      val configModel = orcaRun.orcaSource.model(modelId)
      val modelType = configModel[GridPm.PROP_MODEL_TYPE].get() as ModelType
      val metaModel = orcaRun.orcaSource.model(configModel[GridPm.PROP_META_INFO].get() as ModelId)

      val tableModel = ModelStoreTableModel(orcaRun.orcaSource, modelType, metaModel)
      val table = JTable(tableModel)
      table.autoCreateRowSorter = true

      parent.add(JScrollPane(table))

      val selectionType = GridPm.getSelectionType(configModel.id)

      selectRow(orcaRun.orcaSource, selectionType, table.selectionModel, table.rowSorter, tableModel)

      return selectionIntent(selectionType, table)
    }

    private fun selectRow(orcaSource: OrcaSource, selectionType: ModelType, selectionModel: ListSelectionModel, rowSorter: RowSorter<*>, tableModel: ModelStoreTableModel) {
      orcaSource
              .observeModelStore()
              .filter { it.modelType == selectionType }
              .filter { it.source.isController() }
              .filter { it.eventType.isAdd() }
              .map { orcaSource.model(it.modelId) }
              .subscribe {selectionPm ->
                val rowIndex = tableModel.getIndex(selectionPm[GridPm.PROP_SELECTED_MODEL_ID].getValue())
                val viewIndex = rowSorter.convertRowIndexToView(rowIndex)
//                if (it.eventType.isRemove()) {
//                  FIXME Code cannot handle multi selection
//                  selectionModel.clearSelection()
//                } else {
                  selectionModel.setSelectionInterval(viewIndex, viewIndex)
//                }
              }
    }

    private fun selectionIntent(selectionType: ModelType, table: JTable): Observable<List<CommandData>> {

      fun createSelectionModel(rowId: ModelId): CreateModelCommandData {
        return GridPm.createSelectionPm(rowId, selectionType, Source.UI)
      }

      val subject = PublishSubject.create<List<CommandData>>()
      table.selectionModel.addListSelectionListener {
        if (!it.valueIsAdjusting) {
          val removeOldCommand = RemoveModelByTypeCommandData(Source.UI, selectionType)

          val newSelectedRows = SwingUtil
                  .getSelectedRows(table.selectionModel, table.rowSorter)
                  .map { createSelectionModel((table.model as ModelStoreTableModel).indexToModelId(it)) }

          subject.onNext(listOf(removeOldCommand).plus(newSelectedRows))
        }
      }
      return subject
    }

  }

}

private class ModelStoreTableModel(
        orcaSource: OrcaSource,
        private val modelType: ModelType,
        private val metaModel: PresentationModel) : AbstractTableModel() {


  private val modelList = ArrayList<PresentationModel>()

  init {
    orcaSource.observeModelStore()
            .filter { it.modelType == modelType }
            .subscribe { event ->
              if (event.eventType.isAdd()) {
                modelList.add(orcaSource.model(event.modelId))
                fireTableRowsInserted(modelList.size - 1, modelList.size - 1)
              } else if (event.eventType.isRemove()) {
                val index = modelList.indexOfFirst { it.id == event.modelId }
                modelList.removeAt(index)
                fireTableRowsDeleted(index, index)
              }
            }

    modelList.addAll(orcaSource.models(modelType))
  }

  override fun getRowCount(): Int {
    return modelList.size
  }

  override fun getColumnCount(): Int {
    return metaModel.propertyCount()
  }

  override fun getColumnName(column: Int): String {
    return metaModel.getPropertiesArray()[column][Tag.LABEL].toString()
  }

  override fun getValueAt(rowIndex: Int, columnIndex: Int): Any {
    return modelList[rowIndex][metaModel.getPropertiesArray()[columnIndex].name].get()
  }

  fun indexToModelId(index: Int): ModelId {
    return modelList[index].id
  }

  fun getIndex(modelId: ModelId): Int {
    return modelList.indexOfFirst { it.id == modelId }
  }

}