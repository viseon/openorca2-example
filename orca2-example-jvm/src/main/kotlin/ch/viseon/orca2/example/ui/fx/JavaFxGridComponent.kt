package ch.viseon.orca2.example.ui.fx

//import ch.viseon.orca2.common.*
//import ch.viseon.orca2.example.common.controller.Models
//import ch.viseon.orca2.rx.Observable
//import ch.viseon.orca2.rx.ObservableFactory
//import ch.viseon.orca2.rx.filter
//import ch.viseon.orca2.rx.subscribe
//import com.sun.scenario.effect.impl.prism.PrMergePeer
//import javafx.beans.InvalidationListener
//import javafx.beans.property.ReadOnlyProperty
//import javafx.beans.value.ChangeListener
//import javafx.beans.value.ObservableValue
//import javafx.beans.value.ObservableValueBase
//import javafx.scene.control.Control
//import javafx.scene.control.TableColumn
//import javafx.scene.control.TableView
//import javafx.scene.control.cell.PropertyValueFactory
//import javax.security.auth.callback.Callback
//
//
//class JavaFxGridComponent(private val configModel: PresentationModel,
//                          private val orcaRun: OrcaRun
//) {
//  companion object {
//
//    fun create(configModelId: ModelId, orcaRun: OrcaRun): JavaFxGridComponent {
//      return JavaFxGridComponent(orcaRun.orcaSource.model(configModelId), orcaRun)
//    }
//
//  }
//
//  val component: TableView<PresentationModel>
//  val intentStream: Observable<List<CommandData>>
//
//  init {
//    component = generateView()
//    intentStream = getIntent()
//  }
//
//  private fun getIntent(): Observable<List<CommandData>> {
//    orcaRun.orcaSource.observeModelStore()
//        .filter {
//          val modelType = configModel[Models.GridComponentConfigProperties.PROP_MODEL_TYPE].get() as ModelType
//          it.modelType == modelType
//        }
//        .subscribe {
//          updateModel(it)
//        }
//
//    return ObservableFactory.empty()
//  }
//
//  private fun updateModel(event: ModelStoreChangeEvent) {
//    event
//        .takeIf { it.eventType.isAdd() }
//        ?.let {
//          component.items.add(orcaRun.orcaSource.model(it.modelId))
//        }
//  }
//
//  private fun generateView(): TableView<PresentationModel> {
//    val view = TableView<PresentationModel>()
//
//    val metaPm = orcaRun.orcaSource.model(configModel[Models.GridComponentConfigProperties.PROP_META_INFO].get() as ModelId)
//
//    val columns = metaPm
//        .getProperties()
//        .map { property ->
//          val label = property[Tag.LABEL].toString()
//          TableColumn<PresentationModel, String>(label).apply {
//            setCellFactory {
//              it.
//            }
//          }
//        }
//
//    view.columns.addAll(columns)
//
//    return view
//  }
//
//  object Feature<S, T> : Callback<TableColumn.CellDataFeatures<S, T>, ObservableValue<T>> {
//
//  }
//
//  //Wir geben ein StringProperty o.Ä. zurück für die Werte eines OrcaProperties.
//  //Wenn das Property ein Update erhält müssen wir ein Command absetzen
//  //Wenn das Modell sich ändert, müssen wir das Property update.
//  //Das heisst, wir müssen jedes Property tracken...
//  //Eine Alternative wäre eine eigene Implementierung, die das "binding" vornimt.
//
//
//}