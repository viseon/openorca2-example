package ch.viseon.orca2.example.ui.swing

import ch.viseon.orca2.common.*
import ch.viseon.orca2.example.common.controller.ComponentProperties
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.ObservableOperators
import java.awt.Container
import java.awt.FlowLayout
import javax.swing.JPanel

class SwingToolBar {

  companion object {
    fun create(modelId: ModelId, orcaRun: OrcaRun, componentParent: Container): Observable<List<CommandData>> {

      val model = orcaRun.orcaSource.model(modelId)
      val toolBar = JPanel().apply {
        layout = FlowLayout()
      }

      componentParent.add(toolBar)

      val intents = model
              .getProperties()
              .asSequence()
              .filter { it.hasValue(Tag.VALUE) }
              .map { it[Tag.VALUE] }
              .filter { it is ModelId }
              .map { it as ModelId }
              .filter { orcaRun.orcaSource.contains(it) && orcaRun.orcaSource.model(it).hasProperty(ComponentProperties.PROP_COMPONENT_TYPE) }
              .map { createComponent(orcaRun.orcaSource.model(it), orcaRun, toolBar) }
              .toList()

      return ObservableOperators.merge(*intents.toTypedArray())
    }

    private fun createComponent(model: PresentationModel, orcaRun: OrcaRun, componentParent: Container): Observable<List<CommandData>> {
      return ComponentFactory.createComponent(model, orcaRun, componentParent)
    }

  }

}
