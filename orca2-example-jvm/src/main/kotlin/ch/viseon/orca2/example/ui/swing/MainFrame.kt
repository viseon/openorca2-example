package ch.viseon.orca2.example.ui.swing

import ch.viseon.orca2.common.*
import ch.viseon.orca2.example.common.controller.LoginViewIds
import ch.viseon.orca2.example.common.controller.ViewPm
import ch.viseon.orca2.example.common.controller.asModelId
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.ObservableOperators
import java.awt.Dimension
import javax.swing.JFrame
import javax.swing.WindowConstants


class MainFrame(private val orcaRun: OrcaRun) {

  private val frame = JFrame()

  init {
    configureFrame()
    orcaRun.registerBuggyCompiler {
      registerEvents(it)
    }

  }

  private fun registerEvents(orcaSource: OrcaSource): Observable<List<CommandData>> {
    val routeChangeCmds = orcaSource
            .observeProperty(ViewPm.ID, ViewPm.PROP_ROUTE)
            .map {
              val route: String = it.newValue as String

              val componentId = when (route) {
                "/" -> LoginViewIds.LoginPanel.ID
                "/main-page" -> "mainPanel".asModelId()
                else -> ModelId.EMPTY
              }
              if (componentId == ModelId.EMPTY) {
                println("Component not found for route: $route")
              }
              route to componentId
            }
            .filter { (_, componentType) -> componentType != ModelId.EMPTY }
            .map { (_, componentType) ->
              listOf<CommandData>(CommandUtils.changeValueUI(ViewPm.ID, ViewPm.PROP_COMPONENT, componentType))
            }

    val propChangeCmds = ObservableOperators.merge(
            orcaSource
                    .observeProperty(ViewPm.ID, ViewPm.PROP_COMPONENT)
                    .map { it.source to it.newValue as ModelId },
            orcaSource
                    .observeModelStore()
                    .filter { it.eventType.isAdd() }
                    .filter { it.modelType == ViewPm.TYPE }
                    .filter { it.modelId != ModelId.EMPTY }
                    .map { orcaSource.model(it.modelId) }
                    .map { Source.CONTROLLER to it[ViewPm.PROP_COMPONENT].getValue() as ModelId }
    )
            .filter { (_, id) -> id != ModelId.EMPTY }
            .flatMap { (_, id) ->
              val model = orcaSource.model(id)
              frame.contentPane.removeAll()
              ComponentFactory.createComponent(model, orcaRun, frame.contentPane)
                      .apply {
                        frame.pack()
                      }
            }

    return ObservableOperators.merge(routeChangeCmds, propChangeCmds)
  }

  private fun configureFrame() {
    frame.size = Dimension(800, 600)
    frame.title = "Orca 2 example"
    frame.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
  }

  fun show() {
    frame.isVisible = true
  }

}