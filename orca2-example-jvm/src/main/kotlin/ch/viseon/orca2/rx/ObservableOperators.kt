package ch.viseon.orca2.rx


actual object ObservableOperators {

  actual fun <T> merge(vararg observable: Observable<T>): Observable<T> {
    return io.reactivex.Observable.merge(observable.toList())
  }

}