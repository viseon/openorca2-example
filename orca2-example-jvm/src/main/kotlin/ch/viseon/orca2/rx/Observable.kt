package ch.viseon.orca2.rx

/*
Eine Möglichkeit für PublishSubjects wäre:

Eine eigene Klasse Subject. Diese leite vom orca.Observable ab. Damit stehen alle Filter zur Verfügung.
Die Klasse muss nun pro Plattform implementiert werden.
Dies Klasse benutzt ein eigenes Obserer Interface, das nicht von RxJava oder RxJs stammt.
-> Damit haben wir unser eigenes Subject geschaffen.
 */

actual typealias Observable<T> = io.reactivex.Observable<T>
actual typealias Disposable = io.reactivex.disposables.Disposable
//actual typealias Observer<T> = rx.Observer<T>
//actual typealias Subscriber<T> = rx.Subscriber<T>


actual fun <T> Observable<T>.filter(block: (T) -> Boolean): Observable<T> {
  return this.filter(block)
}

actual fun <T, R> Observable<T>.map(block: (T) -> R): Observable<R> {
  return this.map(block)
}

actual fun <T> Observable<T>.subscribe(block: (T) -> Unit): Disposable {
  return this.subscribe(block)
}

actual fun <T,R> Observable<T>.flatMap(block: (T) -> Observable<R>): Observable<R> {
  return this.flatMap(block)
}

actual fun <T> Observable<T>.subscribe(block: (T) -> Unit, errorBlock: (Throwable) -> Unit): Disposable {
  return this.subscribe(block, errorBlock)
}

actual fun <R> Observable<R>.onErrorResumeNext(block: (Throwable) -> Observable<R>): Observable<R> {
  return this.onErrorResumeNext(block)
}

actual fun <T, R> Observable<T>.reduceToObservable(seed: R, block: (R, T) -> R): Observable<R> {
  return this.reduce(seed, block).toObservable()
}

actual fun <T> Observable<T>.collect(): Observable<List<T>> {
  return this.toList().toObservable()
}
