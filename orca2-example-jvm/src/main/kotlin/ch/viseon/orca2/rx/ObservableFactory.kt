package ch.viseon.orca2.rx

actual object ObservableFactory {

  actual fun <T> create(producer: (CustomSubscriber<T>) -> Unit): Observable<T> {
    return Observable.create { it ->
      val bridge = object: CustomSubscriber<T> {
        override fun onStart() {
        }

        override fun onError(e: Throwable?) {
          it.onError(e!!)
        }

        override fun onCompleted() {
          it.onComplete()
        }

        override fun onNext(t: T) {
          it.onNext(t)
        }

      }
      producer(bridge)
    }
  }

  actual fun <T> empty(): Observable<T> {
    return io.reactivex.Observable.empty()
  }

  actual fun <T> of(t: T): Observable<T> {
    return io.reactivex.Observable.just(t)
  }

}