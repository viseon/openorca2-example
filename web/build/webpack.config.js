'use strict';

var webpack = require('webpack');

var config = {
    "mode": "development",
    "context": "/home/sandro/projects/viseon/orca2-example/web/build/js",
    "entry": {
        "main": "./web"
    },
    "output": {
        "path": "/home/sandro/projects/viseon/orca2-example/web/build/bundle",
        "filename": "[name].bundle.js",
        "chunkFilename": "[id].bundle.js",
        "publicPath": "/"
    },
    "module": {
        "rules": [
            
        ]
    },
    "resolve": {
        "modules": [
            "js",
            "../../orca2-example-js/build/classes/kotlin/main",
            "js/resources",
            "node_modules",
            "/home/sandro/projects/viseon/orca2-example/web/build/node_modules"
        ]
    },
    "plugins": [
        
    ]
};

module.exports = config;

// from file /home/sandro/projects/viseon/orca2-example/web/webpack.config.d/css.js
config.module.rules.push(
    {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
    }


);
