(function (_, Kotlin, $module$orca2_example_js, $module$kotlinx_html_js, $module$rxjs_Rx) {
  'use strict';
  var $$importsForInline$$ = _.$$importsForInline$$ || (_.$$importsForInline$$ = {});
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var controller = $module$orca2_example_js.ch.viseon.orca2.example.common.controller;
  var ImageViewerPm = $module$orca2_example_js.ch.viseon.orca2.binding.components.image.ImageViewerPm;
  var equals = Kotlin.equals;
  var IllegalArgumentException_init = Kotlin.kotlin.IllegalArgumentException_init_pdl1vj$;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var Unit = Kotlin.kotlin.Unit;
  var ul = $module$kotlinx_html_js.kotlinx.html.js.ul_693so7$;
  var div = $module$kotlinx_html_js.kotlinx.html.js.div_wkomt5$;
  var append = $module$kotlinx_html_js.kotlinx.html.dom.append_k9bwru$;
  var throwCCE = Kotlin.throwCCE;
  var ImageViewerElementPm = $module$orca2_example_js.ch.viseon.orca2.binding.components.image.ImageViewerElementPm;
  var img = $module$kotlinx_html_js.kotlinx.html.js.img_6lw7hj$;
  var li = $module$kotlinx_html_js.kotlinx.html.li_jf6zlv$;
  var AbstractImageViewer = $module$orca2_example_js.ch.viseon.orca2.binding.components.image.AbstractImageViewer;
  var div_0 = $module$kotlinx_html_js.kotlinx.html.div_59el9d$;
  var first = Kotlin.kotlin.collections.first_2p1efm$;
  var AbstractToolBar = $module$orca2_example_js.ch.viseon.orca2.binding.components.toolBar.AbstractToolBar;
  var Rx$Subject = $module$rxjs_Rx.Subject;
  var Observable = $module$rxjs_Rx.Observable;
  var ButtonPm = $module$orca2_example_js.ch.viseon.orca2.binding.components.button.ButtonPm;
  var Tag = $module$orca2_example_js.ch.viseon.orca2.common.Tag;
  var button = $module$kotlinx_html_js.kotlinx.html.button_yup7tf$;
  var ensureNotNull = Kotlin.ensureNotNull;
  var Source = $module$orca2_example_js.ch.viseon.orca2.common.Source;
  var listOf = Kotlin.kotlin.collections.listOf_mh5how$;
  var ActionCommandData = $module$orca2_example_js.ch.viseon.orca2.common.ActionCommandData;
  var EventListener = Kotlin.org.w3c.dom.events.EventListener_gbr1zf$;
  var FileSelectionActionPm = $module$orca2_example_js.ch.viseon.orca2.binding.components.file.FileSelectionActionPm;
  var button_0 = $module$kotlinx_html_js.kotlinx.html.js.button_yqfwmz$;
  var InputType = $module$kotlinx_html_js.kotlinx.html.InputType;
  var input = $module$kotlinx_html_js.kotlinx.html.input_e1g74z$;
  var form = $module$kotlinx_html_js.kotlinx.html.form_3vb3wm$;
  var set_onClickFunction = $module$kotlinx_html_js.kotlinx.html.js.set_onClickFunction_pszlq2$;
  var button_1 = $module$kotlinx_html_js.kotlinx.html.button_i4xb7r$;
  var asList = Kotlin.org.w3c.dom.asList_kt9thq$;
  var common = $module$orca2_example_js.ch.viseon.orca2.common;
  var FileUploadResultPm = $module$orca2_example_js.ch.viseon.orca2.binding.components.file.FileUploadResultPm;
  var listOf_0 = Kotlin.kotlin.collections.listOf_i5x0yv$;
  var FormPm = $module$orca2_example_js.ch.viseon.orca2.binding.components.form.FormPm;
  var ModelId = $module$orca2_example_js.ch.viseon.orca2.common.ModelId;
  var get_append = $module$kotlinx_html_js.kotlinx.html.dom.get_append_y4uc6z$;
  var components = $module$orca2_example_js.ch.viseon.orca2.binding.components;
  var label = $module$kotlinx_html_js.kotlinx.html.label_yd75js$;
  var div_1 = $module$kotlinx_html_js.kotlinx.html.div_ri36nr$;
  var form_0 = $module$kotlinx_html_js.kotlinx.html.form_3ereno$;
  var ChangeValueCommandData = $module$orca2_example_js.ch.viseon.orca2.common.ChangeValueCommandData;
  var map = Kotlin.kotlin.sequences.map_z5avom$;
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var plus = Kotlin.kotlin.sequences.plus_9h40j2$;
  var toList = Kotlin.kotlin.sequences.toList_veqyi0$;
  var rx = $module$orca2_example_js.ch.viseon.orca2.rx;
  var GridPm = $module$orca2_example_js.ch.viseon.orca2.binding.components.grid.GridPm;
  var ModelType = $module$orca2_example_js.ch.viseon.orca2.common.ModelType;
  var th = $module$kotlinx_html_js.kotlinx.html.th_bncpyi$;
  var tr = $module$kotlinx_html_js.kotlinx.html.tr_lut1f9$;
  var thead = $module$kotlinx_html_js.kotlinx.html.thead_j1nulr$;
  var Any = Object;
  var td = $module$kotlinx_html_js.kotlinx.html.td_vlzo05$;
  var tr_0 = $module$kotlinx_html_js.kotlinx.html.tr_7wec05$;
  var table = $module$kotlinx_html_js.kotlinx.html.table_dmqmme$;
  var hasClass = Kotlin.kotlin.dom.hasClass_46n0ku$;
  var RemoveModelByTypeCommandData = $module$orca2_example_js.ch.viseon.orca2.common.RemoveModelByTypeCommandData;
  var removeClass = Kotlin.kotlin.dom.removeClass_hhb33f$;
  var addClass = Kotlin.kotlin.dom.addClass_hhb33f$;
  var PanelPm = $module$orca2_example_js.ch.viseon.orca2.binding.components.panel.PanelPm;
  var numberToInt = Kotlin.numberToInt;
  var PanelLayout = $module$orca2_example_js.ch.viseon.orca2.binding.components.panel.PanelLayout;
  var PanelLayout$values = $module$orca2_example_js.ch.viseon.orca2.binding.components.panel.PanelLayout.values;
  var filter = Kotlin.kotlin.sequences.filter_euau3h$;
  var IntRange = Kotlin.kotlin.ranges.IntRange;
  var OrcaSource = $module$orca2_example_js.ch.viseon.orca2.common.OrcaSource;
  var OrcaRun = $module$orca2_example_js.ch.viseon.orca2.common.OrcaRun;
  var NetworkConnection = $module$orca2_example_js.ch.viseon.orca2.common.network.NetworkConnection;
  var ViewPm = $module$orca2_example_js.ch.viseon.orca2.example.common.controller.ViewPm;
  var LoginViewIds = $module$orca2_example_js.ch.viseon.orca2.example.common.controller.LoginViewIds;
  var asModelId = $module$orca2_example_js.ch.viseon.orca2.example.common.controller.asModelId_pdl1vz$;
  var to = Kotlin.kotlin.to_ujzrz7$;
  var clear = Kotlin.kotlin.dom.clear_asww5s$;
  var flatMap = $module$orca2_example_js.ch.viseon.orca2.rx.flatMap;
  HTMLImageViewer.prototype = Object.create(AbstractImageViewer.prototype);
  HTMLImageViewer.prototype.constructor = HTMLImageViewer;
  HTMLToolbar.prototype = Object.create(AbstractToolBar.prototype);
  HTMLToolbar.prototype.constructor = HTMLToolbar;
  function BindingContext(orcaRun, networkConnection) {
    this.orcaRun = orcaRun;
    this.networkConnection = networkConnection;
  }
  Object.defineProperty(BindingContext.prototype, 'orcaSource', {
    get: function () {
      return this.orcaRun.orcaSource;
    }
  });
  BindingContext.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'BindingContext',
    interfaces: []
  };
  function ComponentFactory() {
    ComponentFactory_instance = this;
  }
  ComponentFactory.prototype.createComponent_1lijrg$ = function (model, bindingContext, componentParent) {
    var tmp$;
    var type = model.get_lwfvjv$(controller.ComponentProperties.PROP_COMPONENT_TYPE).get_n11fg5$();
    switch (type) {
      case 'BUTTON':
        tmp$ = HtmlButtonComponent$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        break;
      case 'FORM':
        tmp$ = HtmlFormComponent$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        break;
      case 'PANEL':
        tmp$ = HtmlPanelComponent$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        break;
      case 'GRID':
        tmp$ = HtmlGridComponent$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        break;
      case 'FILE_SELECTION_ACTION':
        tmp$ = HtmlFileSelectionAction$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        break;
      default:if (equals(type, ImageViewerPm.Companion.MODEL_TYPE.stringId))
          tmp$ = HTMLImageViewer$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        else if (equals(type, 'TOOLBAR'))
          tmp$ = HTMLToolbar$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        else
          throw IllegalArgumentException_init('Unknown type: ' + type);
        break;
    }
    return tmp$;
  };
  ComponentFactory.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ComponentFactory',
    interfaces: []
  };
  var ComponentFactory_instance = null;
  function ComponentFactory_getInstance() {
    if (ComponentFactory_instance === null) {
      new ComponentFactory();
    }
    return ComponentFactory_instance;
  }
  function ComponentUtil() {
    ComponentUtil_instance = this;
  }
  ComponentUtil.prototype.generateFieldId_6fk92w$ = function (model, property) {
    return model.id.stringId + '-' + property.name;
  };
  ComponentUtil.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ComponentUtil',
    interfaces: []
  };
  var ComponentUtil_instance = null;
  function ComponentUtil_getInstance() {
    if (ComponentUtil_instance === null) {
      new ComponentUtil();
    }
    return ComponentUtil_instance;
  }
  function HTMLImageViewer(bindingContext) {
    HTMLImageViewer$Companion_getInstance();
    AbstractImageViewer.call(this);
    this.bindingContext_0 = bindingContext;
  }
  Object.defineProperty(HTMLImageViewer.prototype, 'orcaSource', {
    get: function () {
      return this.bindingContext_0.orcaRun.orcaSource;
    }
  });
  function HTMLImageViewer$createContainerElement$lambda$lambda$lambda($receiver) {
    return Unit;
  }
  function HTMLImageViewer$createContainerElement$lambda$lambda(this$) {
    return function ($receiver) {
      ul(this$, void 0, HTMLImageViewer$createContainerElement$lambda$lambda$lambda);
      return Unit;
    };
  }
  function HTMLImageViewer$createContainerElement$lambda($receiver) {
    div($receiver, 'picture-viewer', HTMLImageViewer$createContainerElement$lambda$lambda($receiver));
    return Unit;
  }
  HTMLImageViewer.prototype.createContainerElement_11rb$ = function (containerParent) {
    var tmp$;
    append(containerParent, HTMLImageViewer$createContainerElement$lambda);
    return Kotlin.isType(tmp$ = containerParent.querySelector('.picture-viewer ul'), HTMLUListElement) ? tmp$ : throwCCE();
  };
  function HTMLImageViewer$createImageView$lambda$lambda$lambda(closure$fileContent) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$fileContent.name);
      return Unit;
    };
  }
  function HTMLImageViewer$createImageView$lambda$lambda$lambda_0(closure$fileContent) {
    return function ($receiver) {
      $receiver.src = URL.createObjectURL(closure$fileContent);
      return Unit;
    };
  }
  function HTMLImageViewer$createImageView$lambda$lambda(closure$fileContent, this$) {
    return function ($receiver) {
      div(this$, 'picture-title', HTMLImageViewer$createImageView$lambda$lambda$lambda(closure$fileContent));
      img(this$, void 0, void 0, 'picture-image', HTMLImageViewer$createImageView$lambda$lambda$lambda_0(closure$fileContent));
      return Unit;
    };
  }
  function HTMLImageViewer$createImageView$lambda(closure$fileContent) {
    return function ($receiver) {
      li($receiver, 'picture', HTMLImageViewer$createImageView$lambda$lambda(closure$fileContent, $receiver));
      return Unit;
    };
  }
  var getKClass = Kotlin.getKClass;
  var toString = Kotlin.toString;
  HTMLImageViewer.prototype.createImageView_um91dx$ = function (presentationModel, parent) {
    var $this = presentationModel.get_lwfvjv$(ImageViewerElementPm.Companion.PROP_CONTENT);
    var tag;
    tag = Tag.Companion.VALUE;
    var tmp$, tmp$_0;
    var get = $this.get_n11fg5$(tag);
    tmp$_0 = Kotlin.isType(tmp$ = get, File) ? tmp$ : null;
    if (tmp$_0 == null) {
      throw IllegalArgumentException_init('Value is not of type ' + getKClass(File) + ". Actual value '" + get + "' Type: " + toString(Kotlin.getKClassFromExpression(get).simpleName));
    }
    var fileContent = tmp$_0;
    append(parent, HTMLImageViewer$createImageView$lambda(fileContent));
  };
  function HTMLImageViewer$Companion() {
    HTMLImageViewer$Companion_instance = this;
  }
  HTMLImageViewer$Companion.prototype.create_pvjopt$ = function (modelId, bindingContext, parent) {
    return (new HTMLImageViewer(bindingContext)).createUI_11rb$(parent);
  };
  HTMLImageViewer$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HTMLImageViewer$Companion_instance = null;
  function HTMLImageViewer$Companion_getInstance() {
    if (HTMLImageViewer$Companion_instance === null) {
      new HTMLImageViewer$Companion();
    }
    return HTMLImageViewer$Companion_instance;
  }
  HTMLImageViewer.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HTMLImageViewer',
    interfaces: [AbstractImageViewer]
  };
  function HTMLToolbar(bindingContext) {
    HTMLToolbar$Companion_getInstance();
    AbstractToolBar.call(this);
    this.bindingContext_0 = bindingContext;
  }
  Object.defineProperty(HTMLToolbar.prototype, 'orcaSource', {
    get: function () {
      return this.bindingContext_0.orcaSource;
    }
  });
  function HTMLToolbar$createParent$lambda$lambda($receiver) {
    return Unit;
  }
  function HTMLToolbar$createParent$lambda($receiver) {
    div_0($receiver, 'toolbar', HTMLToolbar$createParent$lambda$lambda);
    return Unit;
  }
  HTMLToolbar.prototype.createParent_11rb$ = function (containerParent) {
    return first(append(containerParent, HTMLToolbar$createParent$lambda));
  };
  HTMLToolbar.prototype.createComponent_my75wm$ = function (model, orcaSource, componentParent) {
    return ComponentFactory_getInstance().createComponent_1lijrg$(model, this.bindingContext_0, componentParent);
  };
  function HTMLToolbar$Companion() {
    HTMLToolbar$Companion_instance = this;
  }
  HTMLToolbar$Companion.prototype.create_pvjopt$ = function (configModelId, bindingContext, parent) {
    return (new HTMLToolbar(bindingContext)).createUI_41oarq$(configModelId, parent);
  };
  HTMLToolbar$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HTMLToolbar$Companion_instance = null;
  function HTMLToolbar$Companion_getInstance() {
    if (HTMLToolbar$Companion_instance === null) {
      new HTMLToolbar$Companion();
    }
    return HTMLToolbar$Companion_instance;
  }
  HTMLToolbar.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HTMLToolbar',
    interfaces: [AbstractToolBar]
  };
  function HtmlButtonComponent() {
    HtmlButtonComponent$Companion_getInstance();
  }
  function HtmlButtonComponent$Companion() {
    HtmlButtonComponent$Companion_instance = this;
  }
  HtmlButtonComponent$Companion.prototype.create_pvjopt$ = function (modelId, bindingContext, parent) {
    var tmp$;
    var subject = new Rx$Subject();
    var configModel = bindingContext.orcaSource.model_i6opkx$(modelId);
    return Kotlin.isType(tmp$ = this.generateView_0(configModel, subject, parent), Observable) ? tmp$ : throwCCE();
  };
  function HtmlButtonComponent$Companion$generateView$lambda$lambda(closure$configModel) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = closure$configModel.id.stringId;
      $receiver_0.put_xwzc9p$('id', value);
      $receiver.unaryPlus_pdl1vz$(closure$configModel.get_lwfvjv$(ButtonPm.Companion.PROP_ACTION).get_n11fg5$(Tag.Companion.LABEL).toString());
      return Unit;
    };
  }
  function HtmlButtonComponent$Companion$generateView$lambda(closure$configModel) {
    return function ($receiver) {
      button($receiver, void 0, void 0, void 0, void 0, void 0, HtmlButtonComponent$Companion$generateView$lambda$lambda(closure$configModel));
      return Unit;
    };
  }
  HtmlButtonComponent$Companion.prototype.generateView_0 = function (configModel, subject, parent) {
    append(parent, HtmlButtonComponent$Companion$generateView$lambda(configModel));
    var buttonNode = ensureNotNull(document.getElementById(configModel.id.stringId));
    buttonNode.addEventListener('click', this.buttonClick_0(configModel, subject));
    return subject;
  };
  function HtmlButtonComponent$Companion$buttonClick$lambda(closure$configModel, closure$subject) {
    return function (it) {
      it.preventDefault();
      var actionName = closure$configModel.get_lwfvjv$(ButtonPm.Companion.PROP_ACTION).get_n11fg5$().toString();
      closure$subject.next(listOf(new ActionCommandData(Source.UI, actionName, listOf(closure$configModel.id))));
      return Unit;
    };
  }
  HtmlButtonComponent$Companion.prototype.buttonClick_0 = function (configModel, subject) {
    return EventListener(HtmlButtonComponent$Companion$buttonClick$lambda(configModel, subject));
  };
  HtmlButtonComponent$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HtmlButtonComponent$Companion_instance = null;
  function HtmlButtonComponent$Companion_getInstance() {
    if (HtmlButtonComponent$Companion_instance === null) {
      new HtmlButtonComponent$Companion();
    }
    return HtmlButtonComponent$Companion_instance;
  }
  HtmlButtonComponent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HtmlButtonComponent',
    interfaces: []
  };
  function HtmlFileSelectionAction() {
    HtmlFileSelectionAction$Companion_getInstance();
  }
  function HtmlFileSelectionAction$Companion() {
    HtmlFileSelectionAction$Companion_instance = this;
  }
  HtmlFileSelectionAction$Companion.prototype.create_pvjopt$ = function (modelId, bindingContext, parent) {
    var tmp$;
    var subject = new Rx$Subject();
    var configModel = bindingContext.orcaSource.model_i6opkx$(modelId);
    return Kotlin.isType(tmp$ = this.generateView_0(configModel, subject, parent, bindingContext.networkConnection), Observable) ? tmp$ : throwCCE();
  };
  function HtmlFileSelectionAction$Companion$generateView$lambda$lambda(closure$configModel) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = closure$configModel.id.stringId;
      $receiver_0.put_xwzc9p$('id', value);
      $receiver.unaryPlus_pdl1vz$(closure$configModel.get_lwfvjv$(FileSelectionActionPm.Companion.PROP_ACTION).get_n11fg5$(Tag.Companion.LABEL).toString());
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$generateView$lambda(closure$configModel) {
    return function ($receiver) {
      button_0($receiver, void 0, void 0, void 0, void 0, void 0, HtmlFileSelectionAction$Companion$generateView$lambda$lambda(closure$configModel));
      return Unit;
    };
  }
  HtmlFileSelectionAction$Companion.prototype.generateView_0 = function (configModel, subject, parent, networkConnection) {
    append(parent, HtmlFileSelectionAction$Companion$generateView$lambda(configModel));
    var buttonNode = ensureNotNull(document.getElementById(configModel.id.stringId));
    buttonNode.addEventListener('click', this.buttonClick_0(configModel, subject, parent, networkConnection));
    return subject;
  };
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda($receiver) {
    var $receiver_0 = $receiver.attributes;
    var value = InputType.file.name;
    $receiver_0.put_xwzc9p$('type', value);
    $receiver.attributes.put_xwzc9p$('accept', 'image/*');
    $receiver.attributes.put_xwzc9p$('name', 'file');
    var $receiver_1 = $receiver.attributes;
    var key = 'multiple';
    $receiver_1.put_xwzc9p$(key, '');
    return Unit;
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda($receiver) {
    var $receiver_0 = $receiver.attributes;
    var value = 'multipart/form-data';
    $receiver_0.put_xwzc9p$('enctype', value);
    input($receiver, void 0, void 0, void 0, void 0, 'file', HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda);
    return Unit;
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda$lambda(closure$parent) {
    return function (it) {
      it.preventDefault();
      removeAll(closure$parent.getElementsByClassName('dialog'));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda_0(closure$parent) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('Close');
      var $receiver_0 = $receiver.attributes;
      var value = 'cancel-button';
      $receiver_0.put_xwzc9p$('id', value);
      set_onClickFunction($receiver, HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda$lambda(closure$parent));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda$lambda_0(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$) {
    return function (it) {
      var tmp$, tmp$_0, tmp$_1;
      it.preventDefault();
      var form = Kotlin.isType(tmp$ = closure$parent.getElementsByClassName('upload-form').item(0), HTMLFormElement) ? tmp$ : throwCCE();
      var fileInput = Kotlin.isType(tmp$_0 = form.querySelector("input[type='file']"), HTMLInputElement) ? tmp$_0 : throwCCE();
      if ((tmp$_1 = fileInput.files) != null) {
        var closure$subject_0 = closure$subject;
        var closure$configModel_0 = closure$configModel;
        var this$HtmlFileSelectionAction$_0 = this$HtmlFileSelectionAction$;
        var closure$parent_0 = closure$parent;
        block$break: do {
          if (tmp$_1.length === 0) {
            break block$break;
          }
          var files = asList(tmp$_1);
          closure$subject_0.next(this$HtmlFileSelectionAction$_0.createCommands_0(files, closure$configModel_0));
          removeAll(closure$parent_0.getElementsByClassName('dialog'));
        }
         while (false);
      }
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda_1(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('OK');
      var $receiver_0 = $receiver.attributes;
      var value = 'close-button';
      $receiver_0.put_xwzc9p$('id', value);
      set_onClickFunction($receiver, HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda$lambda_0(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda_0(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$) {
    return function ($receiver) {
      button_1($receiver, void 0, void 0, void 0, void 0, void 0, HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda_0(closure$parent));
      button_1($receiver, void 0, void 0, void 0, void 0, void 0, HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda_1(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$, this$) {
    return function ($receiver) {
      form($receiver, void 0, void 0, void 0, 'upload-form', HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda);
      div(this$, 'confirmation-bar', HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda_0(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$) {
    return function ($receiver) {
      div($receiver, 'dialog', HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$, $receiver));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$) {
    return function (it) {
      it.preventDefault();
      append(closure$parent, HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$));
      return Unit;
    };
  }
  HtmlFileSelectionAction$Companion.prototype.buttonClick_0 = function (configModel, subject, parent, networkConnection) {
    return EventListener(HtmlFileSelectionAction$Companion$buttonClick$lambda(parent, subject, configModel, this));
  };
  HtmlFileSelectionAction$Companion.prototype.createCommands_0 = function (files, configModel) {
    var actionName = configModel.get_lwfvjv$(FileSelectionActionPm.Companion.PROP_ACTION).get_n11fg5$().toString();
    var resultModelId = FileSelectionActionPm.Companion.getResultId_i6opkx$(configModel.id);
    return listOf_0([common.CommandUtils.changeValueUI_z8gcp9$(resultModelId, FileUploadResultPm.Companion.PROP_FILE_DATA, files), new ActionCommandData(Source.UI, actionName, listOf(resultModelId))]);
  };
  HtmlFileSelectionAction$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HtmlFileSelectionAction$Companion_instance = null;
  function HtmlFileSelectionAction$Companion_getInstance() {
    if (HtmlFileSelectionAction$Companion_instance === null) {
      new HtmlFileSelectionAction$Companion();
    }
    return HtmlFileSelectionAction$Companion_instance;
  }
  HtmlFileSelectionAction.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HtmlFileSelectionAction',
    interfaces: []
  };
  function HtmlFormComponent(bindingContext) {
    HtmlFormComponent$Companion_getInstance();
    this.bindingContext_0 = bindingContext;
  }
  function HtmlFormComponent$Companion() {
    HtmlFormComponent$Companion_instance = this;
  }
  HtmlFormComponent$Companion.prototype.create_pvjopt$ = function (modelId, bindingContext, parent) {
    return (new HtmlFormComponent(bindingContext)).createUI_0(modelId, parent);
  };
  HtmlFormComponent$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HtmlFormComponent$Companion_instance = null;
  function HtmlFormComponent$Companion_getInstance() {
    if (HtmlFormComponent$Companion_instance === null) {
      new HtmlFormComponent$Companion();
    }
    return HtmlFormComponent$Companion_instance;
  }
  HtmlFormComponent.prototype.createUI_0 = function (configModelId, parent) {
    var tmp$;
    this.generateView_0(configModelId, parent);
    return Kotlin.isType(tmp$ = this.getIntent_0(configModelId), Observable) ? tmp$ : throwCCE();
  };
  HtmlFormComponent.prototype.getIntent_0 = function (configModelId) {
    var button = ensureNotNull(document.getElementById('form-action-' + configModelId.stringId));
    var subject = new Rx$Subject();
    button.addEventListener('click', this.formButtonClick_0(configModelId, subject));
    return subject;
  };
  function HtmlFormComponent$generateView$lambda$lambda$lambda$lambda(closure$configModel, closure$it) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = ComponentUtil_getInstance().generateFieldId_6fk92w$(closure$configModel, closure$it);
      $receiver_0.put_xwzc9p$('for', value);
      $receiver.unaryPlus_pdl1vz$(closure$it.get_n11fg5$(Tag.Companion.LABEL).toString());
      return Unit;
    };
  }
  function HtmlFormComponent$generateView$lambda$lambda$lambda$lambda_0(closure$configModel, closure$it) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = ComponentUtil_getInstance().generateFieldId_6fk92w$(closure$configModel, closure$it);
      $receiver_0.put_xwzc9p$('id', value);
      var $receiver_1 = $receiver.attributes;
      var value_0 = 'input-field';
      $receiver_1.put_xwzc9p$('class', value_0);
      return Unit;
    };
  }
  function HtmlFormComponent$generateView$lambda$lambda$lambda(closure$configModel, closure$it) {
    return function ($receiver) {
      label($receiver, void 0, HtmlFormComponent$generateView$lambda$lambda$lambda$lambda(closure$configModel, closure$it));
      input($receiver, void 0, void 0, void 0, void 0, void 0, HtmlFormComponent$generateView$lambda$lambda$lambda$lambda_0(closure$configModel, closure$it));
      return Unit;
    };
  }
  function HtmlFormComponent$generateView$lambda$lambda(closure$configModel) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = 'form-action-' + closure$configModel.id.stringId;
      $receiver_0.put_xwzc9p$('id', value);
      var $receiver_1 = $receiver.attributes;
      var key = 'data-pm-id';
      var value_0 = closure$configModel.id.stringId;
      $receiver_1.put_xwzc9p$(key, value_0);
      $receiver.unaryPlus_pdl1vz$(closure$configModel.get_lwfvjv$(FormPm.Companion.PROP_SUBMIT).get_n11fg5$(Tag.Companion.LABEL).toString());
      return Unit;
    };
  }
  function HtmlFormComponent$generateView$lambda(closure$configModel, closure$dataModel) {
    return function ($receiver) {
      var tmp$;
      var $receiver_0 = $receiver.attributes;
      var value = 'form-' + closure$configModel.id.stringId;
      $receiver_0.put_xwzc9p$('id', value);
      var $receiver_1 = closure$configModel;
      var closure$configModel_0 = closure$configModel;
      if ((tmp$ = closure$configModel_0.hasProperty_lwfvjv$(FormPm.Companion.PROP_RENDER_HINT) && closure$configModel_0.get_lwfvjv$(FormPm.Companion.PROP_RENDER_HINT).hasValue_n11fg5$(components.HTML_TAG) ? $receiver_1 : null) != null) {
        var $receiver_2 = $receiver.attributes;
        var value_0 = tmp$.get_lwfvjv$(FormPm.Companion.PROP_RENDER_HINT).get_n11fg5$(components.HTML_TAG).toString();
        $receiver_2.put_xwzc9p$('class', value_0);
      }
      var $receiver_3 = closure$dataModel.getProperties();
      var tmp$_0;
      tmp$_0 = $receiver_3.iterator();
      while (tmp$_0.hasNext()) {
        var element = tmp$_0.next();
        div_1($receiver, 'form-element', HtmlFormComponent$generateView$lambda$lambda$lambda(closure$configModel, element));
      }
      button_1($receiver, void 0, void 0, void 0, void 0, void 0, HtmlFormComponent$generateView$lambda$lambda(closure$configModel));
      return Unit;
    };
  }
  HtmlFormComponent.prototype.generateView_0 = function (configModelId, parent) {
    var tmp$;
    var configModel = this.bindingContext_0.orcaSource.model_i6opkx$(configModelId);
    var dataModel = this.bindingContext_0.orcaSource.model_i6opkx$(Kotlin.isType(tmp$ = configModel.get_lwfvjv$(FormPm.Companion.PROP_DATA_PM).get_n11fg5$(), ModelId) ? tmp$ : throwCCE());
    form_0(get_append(parent), void 0, void 0, void 0, void 0, HtmlFormComponent$generateView$lambda(configModel, dataModel));
  };
  function HtmlFormComponent$formButtonClick$lambda$lambda(closure$configModel, closure$dataModel) {
    return function (property) {
      var tmp$;
      var elementId = ComponentUtil_getInstance().generateFieldId_6fk92w$(closure$configModel, property);
      var formField = Kotlin.isType(tmp$ = document.getElementById(elementId), HTMLInputElement) ? tmp$ : throwCCE();
      var value = formField.value;
      return new ChangeValueCommandData(Source.UI, closure$dataModel.id, property.name, Tag.Companion.VALUE, value);
    };
  }
  function HtmlFormComponent$formButtonClick$lambda(closure$configModelId, closure$intentSubject) {
    return function (it) {
      var tmp$;
      it.preventDefault();
      var configModel = orcaRun.orcaSource.model_i6opkx$(closure$configModelId);
      var dataModel = orcaRun.orcaSource.model_i6opkx$(Kotlin.isType(tmp$ = configModel.get_lwfvjv$(FormPm.Companion.PROP_DATA_PM).get_n11fg5$(), ModelId) ? tmp$ : throwCCE());
      var changeCommands = map(dataModel.getProperties(), HtmlFormComponent$formButtonClick$lambda$lambda(configModel, dataModel));
      var actionName = configModel.get_lwfvjv$(FormPm.Companion.PROP_SUBMIT).get_n11fg5$().toString();
      println('actionName: ' + actionName);
      closure$intentSubject.next(toList(plus(changeCommands, new ActionCommandData(Source.UI, actionName, listOf(configModel.id)))));
      return Unit;
    };
  }
  HtmlFormComponent.prototype.formButtonClick_0 = function (configModelId, intentSubject) {
    return EventListener(HtmlFormComponent$formButtonClick$lambda(configModelId, intentSubject));
  };
  HtmlFormComponent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HtmlFormComponent',
    interfaces: []
  };
  function HtmlGridComponent(configModel, orcaRun, parent) {
    HtmlGridComponent$Companion_getInstance();
    this.configModel_0 = configModel;
    this.orcaRun_0 = orcaRun;
    this.parent_0 = parent;
    this.selectionSubject_0 = new Rx$Subject();
  }
  function HtmlGridComponent$Companion() {
    HtmlGridComponent$Companion_instance = this;
  }
  HtmlGridComponent$Companion.prototype.create_pvjopt$ = function (configModelId, bindingContext, parent) {
    var tmp$;
    var component = new HtmlGridComponent(bindingContext.orcaSource.model_i6opkx$(configModelId), orcaRun, parent);
    component.generateView_0();
    return rx.ObservableOperators.merge_x917ms$([component.selectionSubject_0, Kotlin.isType(tmp$ = component.getIntent_0(bindingContext.orcaSource), Observable) ? tmp$ : throwCCE()]);
  };
  HtmlGridComponent$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HtmlGridComponent$Companion_instance = null;
  function HtmlGridComponent$Companion_getInstance() {
    if (HtmlGridComponent$Companion_instance === null) {
      new HtmlGridComponent$Companion();
    }
    return HtmlGridComponent$Companion_instance;
  }
  function HtmlGridComponent$getIntent$lambda(this$HtmlGridComponent) {
    return function (it) {
      var tmp$, tmp$_0;
      var modelType = Kotlin.isType(tmp$ = this$HtmlGridComponent.configModel_0.get_lwfvjv$(GridPm.Companion.PROP_MODEL_TYPE).get_n11fg5$(), ModelType) ? tmp$ : throwCCE();
      return (tmp$_0 = it.modelType) != null ? tmp$_0.equals(modelType) : null;
    };
  }
  function HtmlGridComponent$getIntent$lambda_0(this$HtmlGridComponent) {
    return function (it) {
      this$HtmlGridComponent.generateView_0();
      return Unit;
    };
  }
  HtmlGridComponent.prototype.getIntent_0 = function (orcaSource) {
    orcaSource.observeModelStore().filter(HtmlGridComponent$getIntent$lambda(this)).subscribe(HtmlGridComponent$getIntent$lambda_0(this));
    return rx.ObservableFactory.empty_287e2$();
  };
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda$lambda(closure$property) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$property.get_n11fg5$(Tag.Companion.LABEL).toString());
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda(closure$metaModel) {
    return function ($receiver) {
      var tmp$;
      tmp$ = closure$metaModel.getProperties().iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        th($receiver, void 0, void 0, HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda$lambda(element));
      }
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda(closure$metaModel) {
    return function ($receiver) {
      tr($receiver, void 0, HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda(closure$metaModel));
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda$lambda_0(closure$property) {
    return function ($receiver) {
      var $this = closure$property;
      var tag;
      tag = Tag.Companion.VALUE;
      var tmp$, tmp$_0;
      var get = $this.get_n11fg5$(tag);
      tmp$_0 = Kotlin.isType(tmp$ = get, Any) ? tmp$ : null;
      if (tmp$_0 == null) {
        throw IllegalArgumentException_init('Value is not of type ' + getKClass(Any) + ". Actual value '" + get + "' Type: " + toString(Kotlin.getKClassFromExpression(get).simpleName));
      }
      $receiver.unaryPlus_pdl1vz$(tmp$_0.toString());
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda(this$HtmlGridComponent) {
    return function (it) {
      this$HtmlGridComponent.selectionIntent_0(it, this$HtmlGridComponent.selectionSubject_0);
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda_0(closure$metaModel, closure$it, this$HtmlGridComponent) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = 'width: ' + (100 / closure$metaModel.propertyCount() | 0) + '%;';
      $receiver_0.put_xwzc9p$('styles', value);
      var $receiver_1 = $receiver.attributes;
      var value_0 = closure$it.id.stringId;
      $receiver_1.put_xwzc9p$('id', value_0);
      var tmp$;
      tmp$ = closure$it.getProperties().iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        td($receiver, void 0, HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda$lambda_0(element));
      }
      set_onClickFunction($receiver, HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda(this$HtmlGridComponent));
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda(closure$metaModel, closure$models, this$HtmlGridComponent) {
    return function ($receiver) {
      thead($receiver, void 0, HtmlGridComponent$generateView$lambda$lambda$lambda$lambda(closure$metaModel));
      var $receiver_0 = closure$models;
      var tmp$;
      tmp$ = $receiver_0.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        tr_0($receiver, 'grid-row', HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda_0(closure$metaModel, element, this$HtmlGridComponent));
      }
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda(closure$metaModel, closure$models, this$HtmlGridComponent) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = 'person-grid';
      $receiver_0.put_xwzc9p$('id', value);
      table($receiver, void 0, HtmlGridComponent$generateView$lambda$lambda$lambda(closure$metaModel, closure$models, this$HtmlGridComponent));
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda(closure$metaModel, closure$models, this$HtmlGridComponent) {
    return function ($receiver) {
      div_0($receiver, 'grid', HtmlGridComponent$generateView$lambda$lambda(closure$metaModel, closure$models, this$HtmlGridComponent));
      return Unit;
    };
  }
  HtmlGridComponent.prototype.generateView_0 = function () {
    var tmp$, tmp$_0, tmp$_1;
    (tmp$ = document.getElementById('person-grid')) != null ? (tmp$.remove(), Unit) : null;
    var modelType = Kotlin.isType(tmp$_0 = this.configModel_0.get_lwfvjv$(GridPm.Companion.PROP_MODEL_TYPE).get_n11fg5$(), ModelType) ? tmp$_0 : throwCCE();
    var models = this.orcaRun_0.orcaSource.models_2vpu8y$(modelType);
    var metaModel = this.orcaRun_0.orcaSource.model_i6opkx$(Kotlin.isType(tmp$_1 = this.configModel_0.get_lwfvjv$(GridPm.Companion.PROP_META_INFO).get_n11fg5$(), ModelId) ? tmp$_1 : throwCCE());
    append(this.parent_0, HtmlGridComponent$generateView$lambda(metaModel, models, this));
  };
  var ArrayList_init = Kotlin.kotlin.collections.ArrayList_init_287e2$;
  HtmlGridComponent.prototype.selectionIntent_0 = function (it, selectionSubject) {
    var tmp$;
    it.preventDefault();
    var resultCommands = ArrayList_init();
    if ((tmp$ = it.currentTarget) != null) {
      var tmp$_0, tmp$_1, tmp$_2, tmp$_3;
      var selectionType = GridPm.Companion.getSelectionType_i6opkx$(this.configModel_0.id);
      var row = Kotlin.isType(tmp$_0 = tmp$, HTMLElement) ? tmp$_0 : throwCCE();
      if ((tmp$_1 = tmp$.parentElement) != null) {
        var tmp$_4;
        var $receiver = asList(tmp$_1.childNodes);
        var firstOrNull$result;
        firstOrNull$break: do {
          var tmp$_5;
          tmp$_5 = $receiver.iterator();
          while (tmp$_5.hasNext()) {
            var element = tmp$_5.next();
            if (Kotlin.isType(element, HTMLTableRowElement) && hasClass(element, 'grid-row-selected')) {
              firstOrNull$result = element;
              break firstOrNull$break;
            }
          }
          firstOrNull$result = null;
        }
         while (false);
        if ((tmp$_4 = firstOrNull$result) != null) {
          var tmp$_6;
          var $receiver_0 = Kotlin.isType(tmp$_6 = tmp$_4, HTMLElement) ? tmp$_6 : throwCCE();
          resultCommands.add_11rb$(new RemoveModelByTypeCommandData(Source.UI, selectionType));
          removeClass($receiver_0, ['grid-row-selected']);
        }
      }
      addClass(row, ['grid-row-selected']);
      resultCommands.add_11rb$(GridPm.Companion.createSelectionPm_7lnulb$(new ModelId((tmp$_3 = (tmp$_2 = row.attributes['id']) != null ? tmp$_2.value : null) != null ? tmp$_3 : 'ModelId not set as id attribute'), selectionType, Source.UI));
    }
    selectionSubject.next(resultCommands);
  };
  HtmlGridComponent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HtmlGridComponent',
    interfaces: []
  };
  function HtmlPanelComponent(configModelId, bindingContext, parent) {
    HtmlPanelComponent$Companion_getInstance();
    this.configModelId_0 = configModelId;
    this.bindingContext_0 = bindingContext;
    this.parent_0 = parent;
  }
  function HtmlPanelComponent$Companion() {
    HtmlPanelComponent$Companion_instance = this;
  }
  HtmlPanelComponent$Companion.prototype.create_pvjopt$ = function (configModelId, bindingContext, parent) {
    var componet = new HtmlPanelComponent(configModelId, bindingContext, parent);
    return componet.renderViewAndCollectIntent_0();
  };
  HtmlPanelComponent$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HtmlPanelComponent$Companion_instance = null;
  function HtmlPanelComponent$Companion_getInstance() {
    if (HtmlPanelComponent$Companion_instance === null) {
      new HtmlPanelComponent$Companion();
    }
    return HtmlPanelComponent$Companion_instance;
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda$lambda($receiver) {
    return Unit;
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda(closure$layout) {
    return function ($receiver) {
      div_0($receiver, 'panel ' + (closure$layout === PanelLayout.HORIZONTAL ? 'panel-horizontal' : 'panel-vertical'), HtmlPanelComponent$renderViewAndCollectIntent$lambda$lambda);
      return Unit;
    };
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_0(it) {
    return it.hasValue_n11fg5$(Tag.Companion.VALUE);
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_1(it) {
    return it.get_n11fg5$(Tag.Companion.VALUE);
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_2(it) {
    return Kotlin.isType(it, ModelId);
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_3(it) {
    var tmp$;
    return Kotlin.isType(tmp$ = it, ModelId) ? tmp$ : throwCCE();
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_4(it) {
    return orcaRun.orcaSource.contains_i6opkx$(it) && orcaRun.orcaSource.model_i6opkx$(it).hasProperty_lwfvjv$(controller.ComponentProperties.PROP_COMPONENT_TYPE);
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_5(this$HtmlPanelComponent, closure$componentParent) {
    return function (it) {
      return this$HtmlPanelComponent.createComponent_0(orcaRun.orcaSource.model_i6opkx$(it), this$HtmlPanelComponent.bindingContext_0, closure$componentParent);
    };
  }
  var copyToArray = Kotlin.kotlin.collections.copyToArray;
  HtmlPanelComponent.prototype.renderViewAndCollectIntent_0 = function () {
    var tmp$, tmp$_0, tmp$_1;
    var model = orcaRun.orcaSource.model_i6opkx$(this.configModelId_0);
    var layoutValue = (tmp$_1 = (tmp$_0 = Kotlin.isNumber(tmp$ = model.get_lwfvjv$(PanelPm.Companion.PROP_LAYOUT).get_n11fg5$()) ? tmp$ : null) != null ? numberToInt(tmp$_0) : null) != null ? tmp$_1 : 0;
    var layout = PanelLayout$values()[layoutValue];
    var componentParent = first(append(this.parent_0, HtmlPanelComponent$renderViewAndCollectIntent$lambda(layout)));
    var intents = toList(map(filter(map(filter(map(filter(model.getProperties(), HtmlPanelComponent$renderViewAndCollectIntent$lambda_0), HtmlPanelComponent$renderViewAndCollectIntent$lambda_1), HtmlPanelComponent$renderViewAndCollectIntent$lambda_2), HtmlPanelComponent$renderViewAndCollectIntent$lambda_3), HtmlPanelComponent$renderViewAndCollectIntent$lambda_4), HtmlPanelComponent$renderViewAndCollectIntent$lambda_5(this, componentParent)));
    return rx.ObservableOperators.merge_x917ms$(copyToArray(intents).slice());
  };
  HtmlPanelComponent.prototype.createComponent_0 = function (model, bindingContext, componentParent) {
    return ComponentFactory_getInstance().createComponent_1lijrg$(model, bindingContext, componentParent);
  };
  HtmlPanelComponent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HtmlPanelComponent',
    interfaces: []
  };
  function forEach($receiver, block) {
    var tmp$;
    tmp$ = (new IntRange(0, $receiver.length)).iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      var tmp$_0;
      if ((tmp$_0 = $receiver.item(element)) != null) {
        block(tmp$_0);
      }
    }
  }
  function removeAll$lambda(it) {
    it.remove();
    return Unit;
  }
  function removeAll($receiver) {
    forEach($receiver, removeAll$lambda);
  }
  var orcaRun;
  function main$lambda$lambda(closure$bindingContext) {
    return function (it) {
      registerBrowserEvents(closure$bindingContext);
      return registerEvents(closure$bindingContext);
    };
  }
  function main$lambda(closure$networkConnection) {
    return function (it) {
      var bindingContext = new BindingContext(orcaRun, closure$networkConnection);
      orcaRun.register_e7fbwz$(main$lambda$lambda(bindingContext));
      controller.Controller.initialize_5nnjv6$(orcaRun, closure$networkConnection);
      return Unit;
    };
  }
  function main(args) {
    require('style.css');
    require('buffer');
    var networkConnection = new NetworkConnection(controller.Controller.serverUrl);
    window.onload = main$lambda(networkConnection);
  }
  function registerBrowserEvents$lambda(closure$bindingContext) {
    return function (it) {
      var tmp$, tmp$_0;
      var popStateEvent = Kotlin.isType(tmp$ = it, PopStateEvent) ? tmp$ : throwCCE();
      if ((tmp$_0 = popStateEvent.state) != null) {
        var closure$bindingContext_0 = closure$bindingContext;
        var commands = listOf(common.CommandUtils.changeValueUI_z8gcp9$(ViewPm.Companion.ID, ViewPm.Companion.PROP_ROUTE, tmp$_0));
        closure$bindingContext_0.orcaRun.processCommands_krvlx7$(commands);
      }
      return Unit;
    };
  }
  function registerBrowserEvents(bindingContext) {
    window.onpopstate = registerBrowserEvents$lambda(bindingContext);
  }
  function registerEvents$lambda(it) {
    var tmp$, tmp$_0;
    var route = typeof (tmp$ = it.newValue) === 'string' ? tmp$ : throwCCE();
    if (it.source.isController()) {
      window.history.pushState(route, '', route);
    }
    switch (route) {
      case '/':
        tmp$_0 = LoginViewIds.LoginPanel.ID;
        break;
      case '/main-page':
        tmp$_0 = asModelId('mainPanel');
        break;
      default:tmp$_0 = ModelId.Companion.EMPTY;
        break;
    }
    var componentId = tmp$_0;
    if (componentId != null ? componentId.equals(ModelId.Companion.EMPTY) : null) {
      println('Component not found for route: ' + route);
    }
    return to(route, componentId);
  }
  function registerEvents$lambda_0(f) {
    var componentType = f.component2();
    return !(componentType != null ? componentType.equals(ModelId.Companion.EMPTY) : null);
  }
  function registerEvents$lambda_1(f) {
    var componentType = f.component2();
    return listOf(common.CommandUtils.changeValueUI_z8gcp9$(ViewPm.Companion.ID, ViewPm.Companion.PROP_COMPONENT, componentType));
  }
  function registerEvents$lambda_2(it) {
    var tmp$;
    return to(it.source, Kotlin.isType(tmp$ = it.newValue, ModelId) ? tmp$ : throwCCE());
  }
  function registerEvents$lambda_3(it) {
    return it.eventType.isAdd();
  }
  function registerEvents$lambda_4(it) {
    var tmp$;
    return (tmp$ = it.modelType) != null ? tmp$.equals(ViewPm.Companion.TYPE) : null;
  }
  function registerEvents$lambda_5(it) {
    var tmp$;
    return !((tmp$ = it.modelId) != null ? tmp$.equals(ModelId.Companion.EMPTY) : null);
  }
  function registerEvents$lambda_6(closure$bindingContext) {
    return function (it) {
      return closure$bindingContext.orcaSource.model_i6opkx$(it.modelId);
    };
  }
  function registerEvents$lambda_7(it) {
    var tmp$, tmp$_0;
    tmp$_0 = Source.CONTROLLER;
    var $this = it.get_lwfvjv$(ViewPm.Companion.PROP_COMPONENT);
    var tag;
    tag = Tag.Companion.VALUE;
    var tmp$_1, tmp$_0_0;
    var get = $this.get_n11fg5$(tag);
    tmp$_0_0 = Kotlin.isType(tmp$_1 = get, ModelId) ? tmp$_1 : null;
    if (tmp$_0_0 == null) {
      throw IllegalArgumentException_init('Value is not of type ' + getKClass(ModelId) + ". Actual value '" + get + "' Type: " + toString(Kotlin.getKClassFromExpression(get).simpleName));
    }
    return to(tmp$_0, Kotlin.isType(tmp$ = tmp$_0_0, ModelId) ? tmp$ : throwCCE());
  }
  function registerEvents$lambda_8(f) {
    var id = f.component2();
    return !(id != null ? id.equals(ModelId.Companion.EMPTY) : null);
  }
  function registerEvents$lambda_9(closure$bindingContext) {
    return function (f) {
      var source = f.component1()
      , id = f.component2();
      var tmp$;
      var model = closure$bindingContext.orcaSource.model_i6opkx$(id);
      (tmp$ = document.body) != null ? (clear(tmp$), Unit) : null;
      return ComponentFactory_getInstance().createComponent_1lijrg$(model, closure$bindingContext, ensureNotNull(document.body));
    };
  }
  function registerEvents(bindingContext) {
    return rx.ObservableOperators.merge_x917ms$([bindingContext.orcaSource.observeProperty_8zcoiw$(ViewPm.Companion.ID, ViewPm.Companion.PROP_ROUTE).map(registerEvents$lambda).filter(registerEvents$lambda_0).map(registerEvents$lambda_1), flatMap(rx.ObservableOperators.merge_x917ms$([bindingContext.orcaSource.observeProperty_8zcoiw$(ViewPm.Companion.ID, ViewPm.Companion.PROP_COMPONENT).map(registerEvents$lambda_2), bindingContext.orcaSource.observeModelStore().filter(registerEvents$lambda_3).filter(registerEvents$lambda_4).filter(registerEvents$lambda_5).map(registerEvents$lambda_6(bindingContext)).map(registerEvents$lambda_7)]).filter(registerEvents$lambda_8), registerEvents$lambda_9(bindingContext))]);
  }
  _.BindingContext = BindingContext;
  Object.defineProperty(_, 'ComponentFactory', {
    get: ComponentFactory_getInstance
  });
  Object.defineProperty(_, 'ComponentUtil', {
    get: ComponentUtil_getInstance
  });
  $$importsForInline$$['orca2-example-js'] = $module$orca2_example_js;
  Object.defineProperty(HTMLImageViewer, 'Companion', {
    get: HTMLImageViewer$Companion_getInstance
  });
  _.HTMLImageViewer = HTMLImageViewer;
  Object.defineProperty(HTMLToolbar, 'Companion', {
    get: HTMLToolbar$Companion_getInstance
  });
  _.HTMLToolbar = HTMLToolbar;
  Object.defineProperty(HtmlButtonComponent, 'Companion', {
    get: HtmlButtonComponent$Companion_getInstance
  });
  _.HtmlButtonComponent = HtmlButtonComponent;
  Object.defineProperty(HtmlFileSelectionAction, 'Companion', {
    get: HtmlFileSelectionAction$Companion_getInstance
  });
  _.HtmlFileSelectionAction = HtmlFileSelectionAction;
  Object.defineProperty(HtmlFormComponent, 'Companion', {
    get: HtmlFormComponent$Companion_getInstance
  });
  _.HtmlFormComponent = HtmlFormComponent;
  Object.defineProperty(HtmlGridComponent, 'Companion', {
    get: HtmlGridComponent$Companion_getInstance
  });
  _.HtmlGridComponent = HtmlGridComponent;
  Object.defineProperty(HtmlPanelComponent, 'Companion', {
    get: HtmlPanelComponent$Companion_getInstance
  });
  _.HtmlPanelComponent = HtmlPanelComponent;
  _.forEach_qdflk5$ = forEach;
  _.removeAll_sg7yuv$ = removeAll;
  Object.defineProperty(_, 'orcaRun', {
    get: function () {
      return orcaRun;
    }
  });
  _.main_kand9s$ = main;
  _.registerBrowserEvents_br0os6$ = registerBrowserEvents;
  _.registerEvents_br0os6$ = registerEvents;
  orcaRun = new OrcaRun(new OrcaSource(controller.Models.modelStore));
  main([]);
  Kotlin.defineModule('web', _);
  return _;
}(module.exports, require('kotlin'), require('orca2-example-js'), require('kotlinx-html-js'), require('rxjs/Rx')));

//# sourceMappingURL=web.js.map
