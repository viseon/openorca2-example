#Controller
- Think about deregistration of events
- Think about tearDown of views. Controller should be notified.
- Add Icons to Buttons to improve visuals

#General
- Cleanup Code in Views, use abstract classes instead of all compnanions, so we can share code between them. Or maybe use delegation instread of inheritance

#HTML Renderer
- Prevent multiple registration of views
- RxJs 6 Binding instead of Kodando?

#Swing Renderer
- Use MigLayout for better layouting

##Styling
- Rework Image widget
- Rework login page

#Specific to the Photo example

#Server

#ERROR HANDLING!
- Show Error in some dialog or something

#Android Renderer
- Start Android renderer

