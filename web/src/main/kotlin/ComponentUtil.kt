import ch.viseon.orca2.common.PresentationModel
import ch.viseon.orca2.common.Property

object ComponentUtil {

  fun generateFieldId(model: PresentationModel, property: Property): String {
    return "${model.id.stringId}-${property.name}"
  }
}
