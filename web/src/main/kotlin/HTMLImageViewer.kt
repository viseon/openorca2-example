import ch.viseon.orca2.binding.components.ComponentResult
import ch.viseon.orca2.binding.components.image.ImageViewerElementPm
import ch.viseon.orca2.binding.components.image.AbstractImageViewer
import ch.viseon.orca2.common.ModelId
import ch.viseon.orca2.common.OrcaSource
import ch.viseon.orca2.common.PresentationModel
import kotlinx.html.dom.append
import kotlinx.html.js.div
import kotlinx.html.js.img
import kotlinx.html.js.ul
import kotlinx.html.li
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLUListElement
import org.w3c.dom.url.URL
import org.w3c.files.File

class HTMLImageViewer(private val bindingContext: BindingContext): AbstractImageViewer<HTMLElement>() {

  override val orcaSource: OrcaSource
    get() = bindingContext.orcaRun.orcaSource

  override fun createContainerElement(containerParent: HTMLElement): HTMLElement {
    containerParent.append {
      div(classes = "picture-viewer") {
        ul {       }
      }
    }
    return containerParent.querySelector(".picture-viewer ul") as HTMLUListElement
  }

  override fun createImageView(presentationModel: PresentationModel, parent : HTMLElement) {
    val fileContent = presentationModel[ImageViewerElementPm.PROP_CONTENT].getValue<File>()

    parent.append {
      li(classes = "picture") {
        div(classes = "picture-title") {
          +fileContent.name
        }
        img(classes = "picture-image") {
          src = URL.createObjectURL(fileContent)
        }
      }
    }
  }

  companion object {
    fun create(modelId: ModelId, bindingContext: BindingContext, parent: HTMLElement): ComponentResult {
      return HTMLImageViewer(bindingContext).createUI(parent)
    }
  }

}
