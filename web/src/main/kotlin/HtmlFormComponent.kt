import ch.viseon.orca2.binding.components.ComponentResult
import ch.viseon.orca2.binding.components.form.FormPm
import ch.viseon.orca2.binding.components.HTML_TAG
import ch.viseon.orca2.common.*
import ch.viseon.orca2.common.Tag
import ch.viseon.orca2.rx.Observable
import kodando.rxjs.Rx
import kotlinx.html.*
import kotlinx.html.dom.append
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.events.EventListener
import kotlin.browser.document

class HtmlFormComponent(private val bindingContext: BindingContext) {

  companion object {
    fun create(modelId: ModelId, bindingContext: BindingContext, parent: HTMLElement): Observable<List<CommandData>> {
      return HtmlFormComponent(bindingContext).createUI(modelId, parent)
    }
  }

  private fun createUI(configModelId: ModelId, parent: HTMLElement): ComponentResult {
    generateView(configModelId, parent)
    return getIntent(configModelId) as ComponentResult
  }

  private fun getIntent(configModelId: ModelId): Rx.IObservable<List<CommandData>> {
    val button = document.getElementById("form-action-${configModelId.stringId}")!!
    val subject = Rx.Subject<List<CommandData>>()
    button.addEventListener("click", formButtonClick(configModelId, subject))
    return subject
  }

  private fun generateView(configModelId: ModelId, parent: HTMLElement) {
    val configModel = bindingContext.orcaSource.model(configModelId)
    val dataModel = bindingContext.orcaSource.model(configModel[FormPm.PROP_DATA_PM].get() as ModelId)
    parent.append.form {
      attributes["id"] = "form-${configModel.id.stringId}"
      configModel
              .takeIf {
                configModel.hasProperty(FormPm.PROP_RENDER_HINT)
                        && configModel[FormPm.PROP_RENDER_HINT].hasValue(HTML_TAG)
              }
              ?.apply {
                this@form.attributes["class"] = this[FormPm.PROP_RENDER_HINT][HTML_TAG].toString()
              }

      dataModel
              .getProperties()
              .forEach {
                div("form-element") {
                  label {
                    attributes["for"] = ComponentUtil.generateFieldId(configModel, it)
                    +(it[Tag.LABEL].toString())
                  }
                  input {
                    attributes["id"] = ComponentUtil.generateFieldId(configModel, it)
                    attributes["class"] = "input-field"
                  }
                }
              }

      button {
        attributes["id"] = "form-action-${configModel.id.stringId}"
        attributes["data-pm-id"] = configModel.id.stringId
        +configModel[FormPm.PROP_SUBMIT][Tag.LABEL].toString()
      }
    }
  }

  private fun formButtonClick(configModelId: ModelId, intentSubject: Rx.Subject<List<CommandData>>) = EventListener {
    it.preventDefault()

    val configModel = orcaRun.orcaSource.model(configModelId)
    val dataModel = orcaRun.orcaSource.model(configModel[FormPm.PROP_DATA_PM].get() as ModelId)

    val changeCommands = dataModel.getProperties()
            .map { property ->
              val elementId = ComponentUtil.generateFieldId(configModel, property)
              val formField = document.getElementById(elementId) as HTMLInputElement
              val value = formField.value
              ChangeValueCommandData(Source.UI, dataModel.id, property.name, Tag.VALUE, value)
            }

    val actionName = configModel[FormPm.PROP_SUBMIT].get().toString()

    println("actionName: $actionName")
    intentSubject.next(changeCommands.plus(ActionCommandData(Source.UI, actionName, listOf(configModel.id))).toList())
  }

}


