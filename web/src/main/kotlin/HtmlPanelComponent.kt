import ch.viseon.orca2.binding.components.panel.PanelLayout
import ch.viseon.orca2.binding.components.panel.PanelPm
import ch.viseon.orca2.common.CommandData
import ch.viseon.orca2.common.ModelId
import ch.viseon.orca2.common.PresentationModel
import ch.viseon.orca2.common.Tag
import ch.viseon.orca2.example.common.controller.ComponentProperties
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.ObservableOperators
import kotlinx.html.div
import kotlinx.html.dom.append
import org.w3c.dom.HTMLElement

class HtmlPanelComponent(private val configModelId: ModelId,
                         private val bindingContext: BindingContext,
                         private val parent: HTMLElement) {

  companion object {

    fun create(configModelId: ModelId, bindingContext: BindingContext, parent: HTMLElement): Observable<List<CommandData>> {
      val componet = HtmlPanelComponent(configModelId, bindingContext, parent)
      return componet.renderViewAndCollectIntent()
    }
  }

  private fun renderViewAndCollectIntent(): Observable<List<CommandData>> {
    val model = orcaRun.orcaSource.model(configModelId)
    val layoutValue = (model[PanelPm.PROP_LAYOUT].get() as? Number)?.toInt() ?: 0
    val layout = PanelLayout.values()[layoutValue]
    val componentParent = parent.append {
      div(classes = "panel ${ if (layout == PanelLayout.HORIZONTAL) "panel-horizontal" else "panel-vertical"}") { }
    }.first()

    val intents = model
        .getProperties()
            .asSequence()
            .filter { it.hasValue(Tag.VALUE) }
            .map { it[Tag.VALUE] }
            .filter { it is ModelId }
            .map { it as ModelId }
            .filter { orcaRun.orcaSource.contains(it) && orcaRun.orcaSource.model(it).hasProperty(ComponentProperties.PROP_COMPONENT_TYPE)  }
            .map {  createComponent(orcaRun.orcaSource.model(it), bindingContext, componentParent)  }
        .toList()

    return ObservableOperators.merge(*intents.toTypedArray())
  }

  private fun createComponent(model: PresentationModel, bindingContext: BindingContext, componentParent: HTMLElement): Observable<List<CommandData>> {
    return ComponentFactory.createComponent(model, bindingContext, componentParent)
  }


}