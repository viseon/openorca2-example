import org.w3c.dom.Element
import org.w3c.dom.HTMLCollection

fun HTMLCollection.forEach(block: (Element) -> Unit) {
  IntRange(0, this.length).forEach {
    this.item(it)?.let(block)
  }
}

fun HTMLCollection.removeAll() {
  this.forEach {
    it.remove()
  }
}