import ch.viseon.orca2.binding.components.form.FormPm
import ch.viseon.orca2.binding.components.file.FileSelectionActionPm
import ch.viseon.orca2.binding.components.image.ImageViewerPm
import ch.viseon.orca2.common.CommandData
import ch.viseon.orca2.common.PresentationModel
import ch.viseon.orca2.example.common.controller.ComponentProperties
import ch.viseon.orca2.rx.Observable
import org.w3c.dom.HTMLElement


object ComponentFactory {

  fun createComponent(model: PresentationModel, bindingContext: BindingContext,componentParent: HTMLElement): Observable<List<CommandData>> {
    val type = model[ComponentProperties.PROP_COMPONENT_TYPE].get()
    return when (type) {
      "BUTTON" -> {
        HtmlButtonComponent.create(model.id, bindingContext, componentParent)
      }
      FormPm.TYPE -> {
        HtmlFormComponent.create(model.id, bindingContext, componentParent)
      }
      "PANEL" -> {
        HtmlPanelComponent.create(model.id, bindingContext, componentParent)
      }
      "GRID" -> {
        HtmlGridComponent.create(model.id, bindingContext, componentParent)
      }
      FileSelectionActionPm.TYPE -> {
        HtmlFileSelectionAction.create(model.id, bindingContext, componentParent)
      }
      ImageViewerPm.MODEL_TYPE.stringId -> {
        HTMLImageViewer.create(model.id, bindingContext, componentParent)
      }
      "TOOLBAR" -> {
        HTMLToolbar.create(model.id, bindingContext, componentParent)
      }
      else -> throw IllegalArgumentException("Unknown type: $type")
    }
  }

}