import ch.viseon.orca2.binding.components.ComponentResult
import ch.viseon.orca2.binding.components.button.ButtonPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.rx.Observable
import kodando.rxjs.Rx
import kotlinx.html.button
import kotlinx.html.dom.append
import org.w3c.dom.HTMLElement
import org.w3c.dom.events.EventListener
import kotlin.browser.document

class HtmlButtonComponent {

  companion object {
    fun create(modelId: ModelId, bindingContext: BindingContext, parent: HTMLElement): Observable<List<CommandData>> {
      val subject = Rx.Subject<List<CommandData>>()
      val configModel = bindingContext.orcaSource.model(modelId)
      return generateView(configModel, subject, parent) as ComponentResult
    }

    private fun generateView(configModel: PresentationModel, subject: Rx.Subject<List<CommandData>>, parent: HTMLElement): Rx.ISubject<List<CommandData>> {
      parent.append {
        button {
          attributes["id"] = configModel.id.stringId
          +configModel[ButtonPm.PROP_ACTION][Tag.LABEL].toString()
        }
      }

      val buttonNode = document.getElementById(configModel.id.stringId)!!
      buttonNode.addEventListener("click", buttonClick(configModel, subject))
      return subject
    }

    private fun buttonClick(configModel: PresentationModel, subject: Rx.Subject<List<CommandData>>) = EventListener {
      it.preventDefault()

      val actionName = configModel[ButtonPm.PROP_ACTION].get().toString()
      subject.next(listOf(ActionCommandData(Source.UI, actionName, listOf(configModel.id))))
    }

  }
}