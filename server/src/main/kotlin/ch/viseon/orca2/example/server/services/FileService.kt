package ch.viseon.orca2.example.server.services


class FileService : IFileService {

  private val files = mutableMapOf<String, PictureHelper>()

  override fun addFile(fileId: String, metaInfo: Map<FileMetaInfo, Any>, data: ByteArray) {
    files[fileId] = PictureHelper(metaInfo, data)
  }

  override fun getFiles(): Collection<Map.Entry<String, PictureHelper>> {
    return files.entries
  }

}

class PictureHelper(val metaInfo: Map<FileMetaInfo, Any>, val data: ByteArray)

