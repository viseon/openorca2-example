package ch.viseon.orca2.example.server

import ch.viseon.orca2.example.dto.FileDTO
import ch.viseon.orca2.example.server.services.FileMetaInfo
import ch.viseon.orca2.example.server.services.FileType
import ch.viseon.orca2.example.server.services.IFileService
import ch.viseon.orca2.example.server.services.ServiceRegistry
import io.ktor.application.call
import io.ktor.content.PartData
import io.ktor.content.forEachPart
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import kotlinx.io.ByteBuffer
import kotlinx.io.ByteOrder
import java.util.*

fun Routing.pictureApi() {
  post("/picture") {
    val result = call.receiveMultipart()
    result.forEachPart { partData ->
      when (partData) {
        is PartData.FileItem -> {

          val name = partData.contentDisposition?.name
                  ?: throw RuntimeException("Incomplete request, no file id specified")

          val data = partData.streamProvider().use {
            it.readBytes(partData.headers[HttpHeaders.ContentLength]?.toInt() ?: 20 * 1024)
          }
          val metaInfo = mutableMapOf<FileMetaInfo, Any>()
          metaInfo[FileMetaInfo.FILE_NAME] = partData.originalFileName ?: "-UNKNOWN FILENAME-"
          metaInfo[FileMetaInfo.FILE_TYPE] = partData.contentType?.let { FileType.fromMimeType(it.contentSubtype) } ?: FileType.JPEG
          ServiceRegistry[IFileService::class].addFile(name, metaInfo, data)
        }
      }
      partData.dispose()
    }


    println("post picture complete")
    call.respond(HttpStatusCode.OK, "")
  }

  get("/picture") {
    val files = ServiceRegistry[IFileService::class]
            .getFiles()
            .map { (key, picture) ->
              FileDTO(picture.metaInfo[FileMetaInfo.FILE_NAME]?.toString() ?: "",
                      key,
                      (picture.metaInfo[FileMetaInfo.FILE_TYPE] as FileType).toMimeString(),
                      picture.data
              )
            }

    val sumSize = files.sumBy { it.byteSize() } + 4

    val buffer = ByteBuffer.allocate(sumSize)
    buffer.order(ByteOrder.LITTLE_ENDIAN)
    buffer.putInt(files.size)
    files.forEach {
      it.writeToBuffer(buffer)
    }
    buffer.flip()
    val data = buffer.array()
    call.respond(HttpStatusCode.OK, data)
  }

}

private fun ByteArray.toBase64String(): String {
  return Base64.getEncoder().encodeToString(this)
}


//wir müssen im JavaScript ein ArrayBuffer instanzieren.
