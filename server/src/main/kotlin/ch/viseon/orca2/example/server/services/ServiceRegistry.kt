package ch.viseon.orca2.example.server.services

import kotlin.reflect.KClass


object ServiceRegistry {

  private val services = mutableMapOf<KClass<*>, Any>()


  operator fun <T: Any> get(key: KClass<T>): T {
    @Suppress("UNCHECKED_CAST")
    return services[key] as T? ?: throw IllegalArgumentException("No service registered for '$key'")
  }

  fun registerServices(block: ServiceRegistable.() -> Unit) {
    object: ServiceRegistable {
      override fun <T : Any> register(key: KClass<T>, impl: T) {
        services[key] = impl
      }
    }.block()
  }



}

interface ServiceRegistable {
  fun <T: Any> register(key: KClass<T>, impl: T)

}