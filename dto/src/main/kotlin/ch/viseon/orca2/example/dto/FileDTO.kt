package ch.viseon.orca2.example.dto

import kotlinx.io.ByteBuffer
import kotlinx.serialization.Optional
import kotlinx.serialization.SerialId

class FileDTO(@SerialId(1) val fileName: String,
              @SerialId(2) val id: String,
              @SerialId(4) val type: String,
              @SerialId(3) val data: ByteArray) {

  fun writeToBuffer(byteBuffer: ByteBuffer) {
    byteBuffer.writeUTF16String(fileName)
    byteBuffer.writeUTF16String(id)
    byteBuffer.writeUTF16String(type)
    byteBuffer.writeByteArray(data)
  }

  fun byteSize(): Int {
    return fileName.byteSizeUTF16() + id.byteSizeUTF16() + type.byteSizeUTF16() + 4 + data.size
    //Length of array
  }

  fun String.byteSizeUTF16(): Int {
    return this.count() * 2 + 4
  }

  companion object {
    fun fromByteBuffer(byteBuffer: ByteBuffer): FileDTO {
      val fileName = byteBuffer.readUTF16String()
      val id = byteBuffer.readUTF16String()
      val type = byteBuffer.readUTF16String()
      val data = byteBuffer.readByteArray()
      return FileDTO(fileName, id, type, data)
    }

  }

//  @Serializer(forClass = FileDTO::class)
//  companion object : KSerializer<FileDTO> {
//
//    override val serialClassDesc: KSerialClassDesc = SerialClassDescImpl("ch.viseon.orca2.example.dto.FileDTO")
//
//    override fun save(output: KOutput, obj: FileDTO) {
//      output.writeStringValue(obj.fileName)
//      output.writeStringValue(HexConverter.printHexBinary(obj.data))
//    }
//
//    override fun load(input: KInput): FileDTO {
//      return FileDTO(
//              input.readStringValue(),
//              HexConverter.parseHexBinary(input.readStringValue())
//      )
//    }
//  }
}


