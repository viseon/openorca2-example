package ch.viseon.orca2.example.dto

import kotlinx.serialization.Serializable


/**
 * Used for get Request
 */
@Serializable
class Tag(val id: Long, val tagName: String)

