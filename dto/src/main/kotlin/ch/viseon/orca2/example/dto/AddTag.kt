package ch.viseon.orca2.example.dto

import kotlinx.serialization.Serializable

@Serializable
data class AddTagRequest(val tagName: String)

@Serializable
data class AddTagResponse(val id: Long)
