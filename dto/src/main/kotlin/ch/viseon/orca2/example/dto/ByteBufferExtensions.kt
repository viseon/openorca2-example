package ch.viseon.orca2.example.dto

import kotlinx.io.ByteBuffer

fun ByteBuffer.readByteArray(): ByteArray {
  val length = getInt()
  val dest = ByteArray(length)
  this.get(dest, 0, length)
  return dest
}

fun ByteBuffer.readUTF16String(): String {
  val length = getInt()
  val charArray = CharArray(length)
  for (i in 0 until length) {
    charArray[i] = getChar()
  }
  return String(charArray)
}

fun ByteBuffer.writeUTF16String(s: String) {
  putInt(s.length)
  s.forEach {
    putChar(it)
  }
}

fun ByteBuffer.writeByteArray(byteArray: ByteArray) {
  putInt(byteArray.size)
  put(byteArray, 0, byteArray.size)
}
