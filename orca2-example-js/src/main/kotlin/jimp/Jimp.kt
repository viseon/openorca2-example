@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")

package jimp

import org.khronos.webgl.ArrayBuffer
import kotlin.js.Promise

typealias Buffer = ArrayBuffer

external interface Bitmap {
  var data: Buffer
  var width: Number
  var height: Number
}

external interface RGB {
  var r: Number
  var g: Number
  var b: Number
}

external interface RGBA {
  var r: Number
  var g: Number
  var b: Number
  var a: Number
}

external interface `T$0` {
  var r: Array<Number>
  var g: Array<Number>
  var b: Array<Number>
}

external interface `T$1` {
  var percent: Number
  var diff: Jimp
}

@JsModule("jimp")
open external class Jimp {
  constructor(path: String, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */)
  constructor(image: Jimp, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */)
  constructor(data: Buffer, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */)
  constructor(w: Number, h: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */)
  constructor(w: Number, h: Number, background: Number? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */)

  open var bitmap: Bitmap = definedExternally
  open fun clone(cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp = definedExternally
  open fun quality(n: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun deflateLevel(l: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun deflateStrategy(s: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun filterType(f: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun rgba(bool: Boolean, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun background(hex: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun scan(x: Number, y: Number, w: Number, h: Number, f: (x: Number, y: Number, idx: Number) -> Any, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun getMIME(): String = definedExternally
  open fun getExtension(): String = definedExternally
  open fun getPixelIndex(x: Number, y: Number, cb: ((err: Error, i: Number) -> Any)? = definedExternally /* null */): Number = definedExternally
  open fun getPixelColor(x: Number, y: Number, cb: ((err: Error, hex: Number) -> Any)? = definedExternally /* null */): Number = definedExternally
  open fun setPixelColor(hex: Number, x: Number, y: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun hash(base: Number? = definedExternally /* null */, cb: ((err: Error, hash: String) -> Any)? = definedExternally /* null */): String = definedExternally
  open fun crop(x: Number, y: Number, w: Number, h: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun autocrop(tolerance: Number? = definedExternally /* null */, cropOnlyFrames: Boolean? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun blit(src: Jimp, x: Number, y: Number, srcx: Number? = definedExternally /* null */, srcy: Number? = definedExternally /* null */, srcw: Number? = definedExternally /* null */, srch: Number? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun mask(src: Jimp, x: Number, y: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun composite(src: Jimp, x: Number, y: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun brightness(`val`: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun contrast(`val`: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun posterize(n: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun histogram(): `T$0` = definedExternally
  open fun normalize(cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun invert(cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun mirror(horizontal: Boolean, vertical: Boolean, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun gaussian(r: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun blur(r: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun convolution(kernel: Any, edgeHandling: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun convolution(kernel: Any, edgeHandling: (err: Error?, image: Jimp) -> Any, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun greyscale(cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun grayscale(cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun sepia(cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun opacity(f: Any, cb: Any? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun fade(f: Any, cb: Any? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun opaque(cb: Any): Jimp /* this */ = definedExternally
  open fun resize(w: Number, h: Number, mode: String? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun resize(w: Number, h: Number, mode: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun cover(w: Number, h: Number, alignBits: Number? = definedExternally /* null */, mode: String? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun cover(w: Number, h: Number, alignBits: Number? = definedExternally /* null */, mode: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun contain(w: Number, h: Number, alignBits: Number? = definedExternally /* null */, mode: String? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun contain(w: Number, h: Number, alignBits: Number? = definedExternally /* null */, mode: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun scale(f: Number, mode: String? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun scale(f: Number, mode: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun scaleToFit(w: Number, h: Number, mode: Any? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun pixelate(size: Number, x: Number, y: Number, w: Number, h: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun convolute(kernel: Any, x: Number, y: Number? = definedExternally /* null */, w: Number? = definedExternally /* null */, h: Number? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun convolute(kernel: Any, x: (err: Error?, image: Jimp) -> Any, y: Number? = definedExternally /* null */, w: Number? = definedExternally /* null */, h: Number? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun rotate(deg: Number, mode: Number? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun rotate(deg: Number, mode: Boolean? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun displace(map: Jimp, offset: Number, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun getBuffer(mime: String, cb: (err: Error, buffer: Buffer) -> Any): Jimp /* this */ = definedExternally
  open fun getBase64(mime: String, cb: ((err: Error, src: String) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun dither565(cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun dither16(cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun color(actions: Any, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun colour(actions: Any, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun write(path: String, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun print(font: Any, x: Number, y: Number, text: String, maxWidth: Number? = definedExternally /* null */, maxHeight: Number? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun print(font: Any, x: Number, y: Number, text: String, maxWidth: Number? = definedExternally /* null */, maxHeight: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun print(font: Any, x: Number, y: Number, text: String, maxWidth: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */, maxHeight: Number? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun print(font: Any, x: Number, y: Number, text: String, maxWidth: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */, maxHeight: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Jimp /* this */ = definedExternally
  open fun inspect(): String = definedExternally
  override fun toString(): String = definedExternally

  companion object {
    var AUTO: Number = definedExternally
    var MIME_PNG: String = definedExternally
    var MIME_JPEG: String = definedExternally
    var MIME_JGD: String = definedExternally
    var MIME_BMP: String = definedExternally
    var MIME_X_MS_BMP: String = definedExternally
    var MIME_GIF: String = definedExternally
    var PNG_FILTER_AUTO: Number = definedExternally
    var PNG_FILTER_NONE: Number = definedExternally
    var PNG_FILTER_SUB: Number = definedExternally
    var PNG_FILTER_UP: Number = definedExternally
    var PNG_FILTER_AVERAGE: Number = definedExternally
    var PNG_FILTER_PAETH: Number = definedExternally
    var RESIZE_NEAREST_NEIGHBOR: String = definedExternally
    var RESIZE_BILINEAR: String = definedExternally
    var RESIZE_BICUBIC: String = definedExternally
    var RESIZE_HERMITE: String = definedExternally
    var RESIZE_BEZIER: String = definedExternally
    var HORIZONTAL_ALIGN_LEFT: Number = definedExternally
    var HORIZONTAL_ALIGN_CENTER: Number = definedExternally
    var HORIZONTAL_ALIGN_RIGHT: Number = definedExternally
    var VERTICAL_ALIGN_TOP: Number = definedExternally
    var VERTICAL_ALIGN_MIDDLE: Number = definedExternally
    var VERTICAL_ALIGN_BOTTOM: Number = definedExternally
    var FONT_SANS_8_BLACK: String = definedExternally
    var FONT_SANS_10_BLACK: String = definedExternally
    var FONT_SANS_12_BLACK: String = definedExternally
    var FONT_SANS_14_BLACK: String = definedExternally
    var FONT_SANS_16_BLACK: String = definedExternally
    var FONT_SANS_32_BLACK: String = definedExternally
    var FONT_SANS_64_BLACK: String = definedExternally
    var FONT_SANS_128_BLACK: String = definedExternally
    var FONT_SANS_8_WHITE: String = definedExternally
    var FONT_SANS_16_WHITE: String = definedExternally
    var FONT_SANS_32_WHITE: String = definedExternally
    var FONT_SANS_64_WHITE: String = definedExternally
    var FONT_SANS_128_WHITE: String = definedExternally
    var EDGE_EXTEND: Number = definedExternally
    var EDGE_WRAP: Number = definedExternally
    var EDGE_CROP: Number = definedExternally
    fun read(src: String, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Promise<Jimp> = definedExternally
    fun read(src: Buffer, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Promise<Jimp> = definedExternally
    fun loadFont(file: String, cb: ((err: Error?, image: Jimp) -> Any)? = definedExternally /* null */): Promise<Any> = definedExternally
    fun rgbaToInt(r: Number, g: Number, b: Number, a: Number, cb: ((err: Error, i: Number) -> Any)? = definedExternally /* null */): Number = definedExternally
    fun intToRGBA(i: Number, cb: ((err: Error, rgba: RGBA) -> Any)? = definedExternally /* null */): RGBA = definedExternally
    fun limit255(n: Number): Number = definedExternally
    fun diff(img1: Jimp, img2: Jimp, threshold: Number? = definedExternally /* null */): `T$1` = definedExternally
    fun distance(img1: Jimp, img2: Jimp): Number = definedExternally
    fun colorDiff(rgba1: RGBA, rgba2: RGBA): Number = definedExternally
    fun colorDiff(rgba1: RGBA, rgba2: RGB): Number = definedExternally
    fun colorDiff(rgba1: RGB, rgba2: RGBA): Number = definedExternally
    fun colorDiff(rgba1: RGB, rgba2: RGB): Number = definedExternally
  }

  open fun resize(w: Number, h: Number): Jimp /* this */ = definedExternally
  open fun cover(w: Number, h: Number): Jimp /* this */ = definedExternally
  open fun contain(w: Number, h: Number): Jimp /* this */ = definedExternally
  open fun scale(f: Number): Jimp /* this */ = definedExternally
  open fun rotate(deg: Number): Jimp /* this */ = definedExternally
  open fun print(font: Any, x: Number, y: Number, text: String): Jimp /* this */ = definedExternally
}
