#How do I create a binding for a javascript library?

##*.d.ts File is present

Install ts2kt, small npm tool, which will analyze a d.ts file and generate som kotlin classes and such.

In this example I use the library jimp. 

    
    ts2kt jimp.d.ts
    
This will a jimp.Jimp.kt file. (Version 0.1.3)

The auto generated binding is mostly ok, but there are some special cases where we have to put our hands on:

- Inline defined objects like this one


    histogram(): {r: number[], g: number[], b: number[]};
    
Will have special names, like this ``T$0``
    
    
- The original d.ts defines a 


    type ImageCallback = (err: Error|null, image: Jimp) => any;

Which the auto generator ignores and uses a 

    
    ((err: Error?, image: Jimp) -> Any)?
    
    
##No d.ts File is present
Example if there is no *.d.ts file present. As an example, I use the fromBits function from the library sjcl.

A function that is declared this way in JS

    sjcl.codec.bytes = {
        fromBits: function (arr) { /* code omitted */ }
    }
    
Can be called just like this:

    sjcl.codec.hex.fromBits(out);
    

This means the Kotlin declaration has to look like this:

    @file:JsModule("sjcl")
    @file:JsQualifier("codec.hex")

    package sjcl.codec.hex

    /**
     * @param {BitArray} array
     */
    external fun fromBits(array: dynamic): String
    

This will result in the following code:

    //$module$sjcl = require('sjcl');  
    var fromBits = $module$sjcl.codec.hex.fromBits;
    ...
    fromBits(array);
    
    
##Summary

`@JsModule` defines the js module and generates the "require()" code (depending on the module system). Source: https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.js/-js-module/index.html

`external` Tells the compiler that the actual code comes from JS. Source: https://kotlinlang.org/docs/reference/js-interop.html#external-modifier

`dynamic` Tells the compiler that this type is dynamic, that means, we can call everything on it - like in js. Source: https://kotlinlang.org/docs/reference/dynamic-type.html

`@file:JsModule("JS_MODULE")` Applies `@JsModule` to the whole file. Every declaration must now be external.

`@file:JsQualifier("some.js.package")` Applies `@JsQualifier` to the whole file.





    

