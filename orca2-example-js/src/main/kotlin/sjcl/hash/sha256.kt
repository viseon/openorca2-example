@file:JsModule("sjcl")
@file:JsQualifier("hash")
package sjcl.hash

external class sha256 {

  companion object {
    //BitArray|String
    fun hash(data: dynamic): ByteArray
  }


}

