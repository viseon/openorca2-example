package ch.viseon.orca2.rx

import kodando.rxjs.Rx

actual object ObservableFactory {

  actual fun <T> create(producer: (CustomSubscriber<T>) -> Unit): Observable<T> {
    return Rx.Observable.create { observer: Rx.IObserver<T> ->
      val bridge = object : CustomSubscriber<T> {
        override fun onStart() {
          //start not supported
        }

        override fun onError(error: Throwable?) {
          observer.error(error.asDynamic())
        }

        override fun onCompleted() {
          observer.complete()
        }

        override fun onNext(t: T) {
          observer.next(t)
        }

      }
      producer(bridge)
    }.asDynamic()
  }

  actual fun <T> empty(): Observable<T> {
    return Rx.Observable.empty<T>().asDynamic()
  }

  actual fun <T> of(t: T): Observable<T> {
    return Rx.Observable.of<T>(t).asDynamic()
  }

}