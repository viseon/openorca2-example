package ch.viseon.orca2.rx

import kodando.rxjs.Rx
import kodando.rxjs.map
import kodando.rxjs.toArray

@JsName("filter")
actual inline fun <T> Observable<T>.filter(block: (T) -> Boolean): Observable<T> {
  return this.asDynamic().filter(block.asDynamic())
}

@JsName("map")
actual inline fun <T, R> Observable<T>.map(block: (T) -> R): Observable<R> {
  return this.asDynamic().map(block.asDynamic())
}

@Suppress("EXTENSION_SHADOWED_BY_MEMBER")
@JsName("subscribe")
actual fun <T> Observable<T>.subscribe(block: (T) -> Unit): Disposable {
  val subscription = this.asDynamic().subscribe(block.asDynamic())
  return DisposableWrapper(subscription)
}

@Suppress("EXTENSION_SHADOWED_BY_MEMBER")
actual fun <T> Observable<T>.subscribe(block: (T) -> Unit, errorBlock: (Throwable) -> Unit): Disposable {
  val subscription = this.asDynamic()
          .subscribe(
                  block.asDynamic(),
                  errorBlock.asDynamic()
          )
  return DisposableWrapper(subscription)
}

@JsName("flatMap")
actual fun <T, R> Observable<T>.flatMap(block: (T) -> Observable<R>): Observable<R> {
  return this.asDynamic().flatMap(block.asDynamic())
}

actual interface Disposable {
  actual fun dispose()
  actual fun isDisposed(): Boolean
}

class DisposableWrapper(private val subscription: Rx.Subscription) : Disposable {

  private var disposed = false

  override fun dispose() {
    disposed = true
    subscription.unsubscribe()
  }

  override fun isDisposed(): Boolean {
    return disposed
  }

}

@JsName("onErrorResumeNext")
actual fun <R> Observable<R>.onErrorResumeNext(block: (Throwable) -> Observable<R>): Observable<R> {
  return this.asDynamic().catch { error: Throwable, obs: dynamic ->
    block(error)
  }
}

actual fun <T, R> Observable<T>.reduceToObservable(seed: R, block: (R, T) -> R): Observable<R> {
  return this.asDynamic().reduce(block, seed)
}

actual fun <T> Observable<T>.collect(): Observable<List<T>> {
  return this.toArray().map { it.toList() }.asDynamic()
}
