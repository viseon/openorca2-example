package ch.viseon.orca2.common.util

import org.w3c.files.FilePropertyBag

actual object PlatformFileUtil {

  actual fun sha256(fileData: ByteArray): String {
    val hashBytes = sjcl.hash.sha256.hash(fileData)
    return sjcl.codec.hex.fromBits(hashBytes)
  }

  actual fun createFile(data: ByteArray, fileName: String, fileType: String): File {
    return org.w3c.files.File(arrayOf(data.asDynamic()), fileName, FilePropertyBag(type = fileType))
  }
}

actual fun myPrintStackTrace(throwable: Throwable) {
  val stack = throwable.asDynamic().stack
  console.log("${throwable::class}: ${throwable.message}\n$stack\n${throwable.cause?.let { "Cause by\n" }}${throwable.cause?.apply { myPrintStackTrace(this)} ?: ""}")
}