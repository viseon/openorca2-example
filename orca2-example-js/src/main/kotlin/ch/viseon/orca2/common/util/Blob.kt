package ch.viseon.orca2.common.util

import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.ObservableFactory
import ch.viseon.orca2.rx.map
import kodando.rxjs.Rx
import kotlinx.io.ByteBuffer
import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Int8Array
import org.w3c.files.FileReader

/**
 * Represents an abstraction over a file.
 */
actual typealias Blob = org.w3c.files.Blob

actual fun Blob.useAsByteArray(block: (ByteArray) -> Unit) {
  this.useAsArrayBuffer {
    block(Int8Array(it).asDynamic())
  }
}

fun Blob.useAsArrayBuffer(block: (ArrayBuffer) -> Unit) {
  val fileReader = FileReader()
  fileReader.onload = {
    block(fileReader.result as ArrayBuffer)
  }
  fileReader.readAsArrayBuffer(this)
}

actual typealias File = org.w3c.files.File

actual fun Blob.emitByteArray(): Observable<ByteArray> {
  return emitArrayBuffer()
          .map {
            Int8Array(it).asDynamic()
          }
}

fun Blob.emitArrayBuffer(): Observable<ArrayBuffer> {
  return ObservableFactory.create { subscriber ->
    val fileReader = FileReader()
    fileReader.onload = {
      subscriber.onNext(fileReader.result as ArrayBuffer)
    }
    fileReader.readAsArrayBuffer(this)
  }
}